﻿using UnityEngine;

public class BackdropObjectSpawner : PooledObject {

    [SerializeField]
    public FloatRange timeBetweenSpawns, scale;
    [SerializeField]
    public float currentSpeed;
    [SerializeField]
    public BackdropObject[] objectPrefabs;
    [SerializeField]
    float timeSinceLastSpawn;
    [SerializeField]
    float currentSpawnDelay;
    
    [SerializeField]
    public int ZoneArrayIndex { get; set; }
    [SerializeField]
    public float PreTransitionMaxInterval { get; set; }
    [SerializeField]
    public float PreTransitionMinInterval { get; set; }
    [SerializeField]
    public float PreTransitionMaxScale { get; set; }

    [SerializeField]
    private bool PreSpawning = false;

    void FixedUpdate()
    {
        if (!PreSpawning)
        {
            timeSinceLastSpawn += Time.deltaTime;
            if (timeSinceLastSpawn >= currentSpawnDelay)
            {
                timeSinceLastSpawn -= currentSpawnDelay;
                currentSpawnDelay = timeBetweenSpawns.RandomInRange;
                SpawnObject();
            }
        }
        
    }

    void SpawnObject () {
        int spawningObjectIndex = Random.Range(0, objectPrefabs.Length);
        BackdropObject prefab = objectPrefabs[spawningObjectIndex];
		BackdropObject spawn = prefab.GetPooledInstance<BackdropObject>();

        spawn.transform.localPosition = transform.position;
        Vector3 scaleMultiplier = Vector3.one * scale.RandomInRange;
        scaleMultiplier.Scale(objectPrefabs[spawningObjectIndex].transform.localScale);
        spawn.transform.localScale = scaleMultiplier;
        spawn.setSpeed(currentSpeed);
    }

    private BackdropObject SpawnObject(Vector3 spawnLocation)
    {
        int spawningObjectIndex = Random.Range(0, objectPrefabs.Length);

        BackdropObject prefab = objectPrefabs[spawningObjectIndex];
        BackdropObject spawn = prefab.GetPooledInstance<BackdropObject>();

        spawn.transform.localPosition = spawnLocation;
        Vector3 scaleMultiplier = Vector3.one * scale.RandomInRange;
        scaleMultiplier.Scale(objectPrefabs[spawningObjectIndex].transform.localScale);
        spawn.transform.localScale = scaleMultiplier;
        spawn.setSpeed(0f);
        return spawn;
    }

    void OnEnable ()
    {
        currentSpawnDelay = timeBetweenSpawns.RandomInRange;
    }

    void Start()
    {
        currentSpawnDelay = timeBetweenSpawns.RandomInRange;
    }

    public void PreSpawn(float StartingWidth)
    {
        PreSpawning = true;
        currentSpawnDelay = timeBetweenSpawns.RandomInRange;

        float currentDistance = 0f;
        Vector3 currentPosition = transform.position;

        while (currentDistance < StartingWidth)
        {

            BackdropObject tempObject = SpawnObject(currentPosition);
            if (timeBetweenSpawns.max == timeBetweenSpawns.min)
            {
                currentDistance = currentDistance + tempObject.GetComponentInChildren<Renderer>().bounds.size.x;
            }else
            {
                currentDistance = currentDistance + currentSpawnDelay + tempObject.GetComponentInChildren<Renderer>().bounds.size.x;
            }
            currentPosition.x = currentPosition.x + currentDistance;
            
            currentSpawnDelay = timeBetweenSpawns.RandomInRange;
        }

        currentSpeed = 0f;
    }
}