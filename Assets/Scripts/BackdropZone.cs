﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class BackdropZone : PooledObject {

    //An easily referred-to name for this zone
    [SerializeField]
    private string ZoneName;
	[SerializeField]
	private FloatRange ZoneLengthRange;
	[SerializeField]
	private float ZoneLength;


    
    [SerializeField]
    private FloatRange TransitionLength;
    [SerializeField]
    private float TransitionInMultiplier;
    [SerializeField]
    private float TransitionOutMultiplier;


    [SerializeField]
    public Material groundPlaneMaterial;

    [SerializeField]
    public ObjectSpawner[] Spawners;

    [SerializeField]
    private bool TransitionIn;
    [SerializeField]
    private bool TransitionOut;
    [SerializeField]
    private float TransitionInTime;
    [SerializeField]
    private float TransitionOutTime;
    [SerializeField]
    private float TimeSinceTransitionInStart;
    [SerializeField]
    private float TimeSinceTransitionOutStart;
    [SerializeField]
    private List<BackdropObjectSpawner> currentSpawners;
    


    // Update is called once per frame
    void Update()
    {
        
		//If we're transitioning in, call the transition in step
        if (TransitionIn)
        {
            TransitionInStep();
        }

		//If we're transitioning out, call the transition out step
        if (TransitionOut)
        {
            TransitionOutStep();
        }


    }

    //Set values for transition in
    public void StartTransitionIn()
    {

        //Debug.Log(ZoneName + " Transition In Started");


        //Reset TimeSinceTransitionOutStart 
        TimeSinceTransitionOutStart = 0f;

        //If we don't already have a transition time, generate one
        if (TransitionInTime == 0f)
        {
            TransitionInTime = TransitionLength.RandomInRange;
        }

        //Debug.Log(ZoneName + " TransitionInTime is " + TransitionInTime);

        //Create a new temporary list to hold our BackdropObjectSpawners as we create them
        List<BackdropObjectSpawner> newSpawners = new List<BackdropObjectSpawner>();

        foreach(ObjectSpawner spawner in Spawners)
        {
            //Get a spawner from the pool based on the prefab
            BackdropObjectSpawner prefab = spawner.Spawner;
            BackdropObjectSpawner spawn = prefab.GetPooledInstance<BackdropObjectSpawner>();

            //Add it to our temporary list
            newSpawners.Add(spawn);
        }

        currentSpawners = newSpawners;


        //Iterate through the new currentSpawners structs        
        int count = new int();
        count = currentSpawners.Count;
        for (int i = 0; i < count; i++)
        {

			
            BackdropObjectSpawner spawn = currentSpawners[i];            

            spawn.ZoneArrayIndex = i;

            if(Spawners[i].timeBetweenSpawns.max != Spawners[i].timeBetweenSpawns.min)
            {
                spawn.timeBetweenSpawns.max = (Spawners[i].timeBetweenSpawns.max * TransitionInMultiplier);
                spawn.timeBetweenSpawns.min = (Spawners[i].timeBetweenSpawns.max * TransitionInMultiplier);
            }else
            {
                spawn.timeBetweenSpawns.max = Spawners[i].timeBetweenSpawns.max;
                spawn.timeBetweenSpawns.min = Spawners[i].timeBetweenSpawns.min;
            }

            if (Spawners[i].scale.max != Spawners[i].scale.min)
            {
                spawn.scale.max = Spawners[i].scale.min;
                
            }
            else
            {
                spawn.scale.max = Spawners[i].scale.max;
                
            }
            spawn.scale.min = Spawners[i].scale.min;


            spawn.transform.position = Spawners[i].position;
            //Debug.Log("Spawner placed at " + spawn.transform.position.ToString());

            spawn.PreTransitionMaxInterval = spawn.timeBetweenSpawns.max;
            //Debug.Log("PreTransitionMaxInterval for spawner " + i + " set to " + spawn.PreTransitionMaxInterval);
            spawn.PreTransitionMinInterval = spawn.timeBetweenSpawns.min;
            //Debug.Log("PreTransitionMinInterval for spawner " + i + " set to " + spawn.PreTransitionMinInterval);
            spawn.PreTransitionMaxScale = spawn.scale.max;
            //Debug.Log("PreTransitionMaxScale for spawner " + i + " set to " + spawn.PreTransitionMaxScale);

        }

        //Debug.Log("newSpawners size is " + newSpawners.Count);

        //currentSpawners = newSpawners;

        //Debug.Log("CurrentSpawners size is " + currentSpawners.Count);

        TransitionOut = false;
        TransitionIn = true;

        //Debug.Log("StartTransitionIn set TransitionIn to " + TransitionIn);

    }

    //Set values for transition out
    public void StartTransitionOut()
    {
        
        //Debug.Log(ZoneName + " Transition Out Started");

        int count = new int();
        count = currentSpawners.Count;
        for (int i = 0; i < count; i++)
        {
            currentSpawners[i].PreTransitionMaxInterval = currentSpawners[i].timeBetweenSpawns.max;
            currentSpawners[i].PreTransitionMinInterval = currentSpawners[i].timeBetweenSpawns.min;
            currentSpawners[i].PreTransitionMaxScale = currentSpawners[i].scale.max;
        }
        


        //if Transition time hasn't been set, set it
        if (TransitionOutTime == 0f)
        {
            //Debug.Log("TransitionOutTime not set. Setting.");
            TransitionOutTime = TransitionLength.RandomInRange;
        }

        //Debug.Log("TransitionOutTime set to " + TransitionOutTime);

        TimeSinceTransitionOutStart = 0f;
        
        TransitionOut = true;
        TransitionIn = false;

        //Debug.Log("StartTransitionOut set TransitionOut to " + TransitionOut);

    }


    private void TransitionInStep()
    {
        //Debug.Log(ZoneName + " TransitionInTime is " + TransitionInTime);
        //Increment TimeSinceTransitionInStart
        TimeSinceTransitionInStart = TimeSinceTransitionInStart + (Time.deltaTime * 1f);
        //Debug.Log(ZoneName + " TimeSinceTransitionInStart is " + TimeSinceTransitionInStart);

        bool TargetMinTimeReached = false;
        bool TargetMaxTimeReached = false;
        bool targetScaleReached = false;

        int count = new int();
        count = currentSpawners.Count;
        for (int i = 0; i < count; i++)
        {

            //Debug.Log("Running increments for object " + i);
            //Set transition values assuming this loop will need to be run again
            TransitionIn = true;
            TargetMinTimeReached = false;
            TargetMaxTimeReached = false;
            targetScaleReached = false;

            int currentSpawnerZoneArrayIndex = currentSpawners[i].ZoneArrayIndex;
            float PreTransitionMinInterval = currentSpawners[i].PreTransitionMinInterval;
            float PreTransitionMaxInterval = currentSpawners[i].PreTransitionMaxInterval;
            float PreTransitionMaxScale = currentSpawners[i].PreTransitionMaxScale;

            float TargetMinInterval = Spawners[currentSpawnerZoneArrayIndex].timeBetweenSpawns.min;
            float TargetMaxInterval = Spawners[currentSpawnerZoneArrayIndex].timeBetweenSpawns.max;
            float TargetMaxScale = Spawners[currentSpawnerZoneArrayIndex].scale.max;

            // tempColor = Color.Lerp(OldGround.color, NewColor, (Mathf.Pow(GroundCurve.Evaluate(TimeSinceTransitionStart / CurrentTransitionTime),2.0f)));
            //Evaluate TransitionIn curve
            float CurveEvaluation = Mathf.Pow(Spawners[currentSpawnerZoneArrayIndex].TransitionInCurve.Evaluate(TimeSinceTransitionInStart / TransitionInTime), 2.0f);

            //float CurveEvaluation = Spawners[currentSpawnerZoneArrayIndex].TransitionInCurve.Evaluate(TimeSinceTransitionInStart / TransitionInTime);
            //Debug.Log("Calculated CurveEvaluation for spawner " + i + " min is " + CurveEvaluation);

            //If the target min and max time *AREN'T* supposed to be the same
            if (Spawners[currentSpawnerZoneArrayIndex].timeBetweenSpawns.min != Spawners[currentSpawnerZoneArrayIndex].timeBetweenSpawns.max)
            {

                //If the target min time is not yet reached
                if (currentSpawners[i].timeBetweenSpawns.min > Spawners[currentSpawnerZoneArrayIndex].timeBetweenSpawns.min)
                {
                    //Debug.Log("Min spawn gap for spawner " + i + " not yet reached.");
                    //Lerp the two based on the animation curve

                    //Calculate Lerp value based on curve
                    float lerp = Mathf.Lerp(PreTransitionMinInterval, TargetMinInterval, CurveEvaluation);
                    //Debug.Log("Calculated lerp for spawner " + i + " min is " + lerp);

                    //Set to lerp               
                    currentSpawners[i].timeBetweenSpawns.min = lerp;
                }
                //In case min time is below target
                else if (currentSpawners[i].timeBetweenSpawns.min < Spawners[currentSpawnerZoneArrayIndex].timeBetweenSpawns.min)
                {
                    //set min time to target
                    currentSpawners[i].timeBetweenSpawns.min = Spawners[currentSpawnerZoneArrayIndex].timeBetweenSpawns.min;
                }

                //Debug.Log("Min spawn gap for spawner " + i + " = " + currentSpawners[i].timeBetweenSpawns.min);
                //Debug.Log("Target min spawn gap for spawner " + i + " = " + Spawners[currentSpawnerZoneArrayIndex].timeBetweenSpawns.min);
                //Debug.Log("Distance to target min spawn gap for spawner " + i + " = " + (currentSpawners[i].timeBetweenSpawns.min - Spawners[currentSpawnerZoneArrayIndex].timeBetweenSpawns.min));

                //If the target max time is not yet reached
                if (currentSpawners[i].timeBetweenSpawns.max > Spawners[currentSpawnerZoneArrayIndex].timeBetweenSpawns.max)
                {
                    //Debug.Log("Max spawn gap for spawner " + i + " not yet reached.");
                    //Lerp the two based on the animation curve

                    //Calculate Lerp value based on curve
                    float lerp = Mathf.Lerp(PreTransitionMaxInterval, TargetMaxInterval, CurveEvaluation);
                    //Debug.Log("Calculated lerp for spawner " + i + " max is " + lerp);

                    //Set to lerp
                    currentSpawners[i].timeBetweenSpawns.max = lerp;
                }
                //In case max time is below target
                else if (currentSpawners[i].timeBetweenSpawns.max < Spawners[currentSpawnerZoneArrayIndex].timeBetweenSpawns.max)
                {
                    //set max time to target
                    currentSpawners[i].timeBetweenSpawns.max = Spawners[currentSpawnerZoneArrayIndex].timeBetweenSpawns.max;
                }

                //Debug.Log("Max spawn gap for spawner " + i + " = " + currentSpawners[i].timeBetweenSpawns.max);
                //Debug.Log("Target max spawn gap for spawner " + i + " = " + Spawners[currentSpawnerZoneArrayIndex].timeBetweenSpawns.max);
                //Debug.Log("Distance to target max spawn gap for spawner " + i + " = " + (currentSpawners[i].timeBetweenSpawns.max - Spawners[currentSpawnerZoneArrayIndex].timeBetweenSpawns.max));
            }
            else
            {
                //Debug.Log("Detected that min and max spawn time are always the same for spawner " + currentSpawnerZoneArrayIndex);
            }


            //If max and min spawn *AREN'T* supposed to be identical...
            if (Spawners[currentSpawnerZoneArrayIndex].scale.max != Spawners[currentSpawnerZoneArrayIndex].scale.min)
            {
                if (currentSpawners[i].scale.max < Spawners[currentSpawnerZoneArrayIndex].scale.max)
                {
                    //Debug.Log("Max scale for spawner " + i + " not yet reached.");
                    //Lerp the two based on the animation curve

                    //Calculate Lerp value based on curve
                    float lerp = Mathf.Lerp(PreTransitionMaxScale, TargetMaxScale, CurveEvaluation);
                    //Debug.Log("Calculated lerp for spawner " + i + " max scale is " + lerp);

                    //Set to lerp
                    currentSpawners[i].scale.max = lerp;
                }
                

            //    Debug.Log("Max spawn scale for spawner " + i + " = " + currentSpawners[i].scale.max);
            //    Debug.Log("Target max scale for spawner " + i + " = " + Spawners[currentSpawnerZoneArrayIndex].scale.max);
            //    Debug.Log("Distance to target max scale for spawner " + i + " = " + (currentSpawners[i].scale.max - Spawners[currentSpawnerZoneArrayIndex].scale.max));
            //} else
            //{
            //    Debug.Log("Detected that min and max scale are always the same for spawner " + currentSpawnerZoneArrayIndex);
            }

            if (currentSpawners[i].scale.max > Spawners[currentSpawnerZoneArrayIndex].scale.max)
            {
                currentSpawners[i].scale.max = Spawners[currentSpawnerZoneArrayIndex].scale.max;
            }


            if (currentSpawners[i].timeBetweenSpawns.min == Spawners[currentSpawnerZoneArrayIndex].timeBetweenSpawns.min)
            {
                //Debug.Log("Min time for spawner " + i + " now at target");
                TargetMinTimeReached = true;
            }

            if (currentSpawners[i].timeBetweenSpawns.max == Spawners[currentSpawnerZoneArrayIndex].timeBetweenSpawns.max)
            {
                //Debug.Log("Max time for spawner " + i + " now at target");
                TargetMaxTimeReached = true;
            }

            if (currentSpawners[i].scale.max == Spawners[currentSpawnerZoneArrayIndex].scale.max)
            {
                //Debug.Log("Max scale for spawner " + i + " now at target");
                targetScaleReached = true;
            }

            //If min time AND min scale are both at target, set values to end the transition in, on the assumption this is the last object checked
            if (TargetMinTimeReached && TargetMaxTimeReached && targetScaleReached)
            {
                TransitionIn = false;
                //Debug.Log(ZoneName + " Transition In Completed Normally");
                //Debug.Log("Min and max spawn and scale for spawner " + i + " now all at targets");
            }

            
        }

        //TransitionIn = false;

        //count = new int();
        //count = currentSpawners.Count;
        //for (int i = 0; i < count; i++)
        //{
        //    int currentSpawnerZoneArrayIndex = currentSpawners[i].ZoneArrayIndex;
        //    if (!(currentSpawners[i].timeBetweenSpawns.min == Spawners[currentSpawnerZoneArrayIndex].timeBetweenSpawns.min) || !(currentSpawners[i].timeBetweenSpawns.max == Spawners[currentSpawnerZoneArrayIndex].timeBetweenSpawns.max) || !(currentSpawners[i].scale.max == Spawners[currentSpawnerZoneArrayIndex].scale.max))
        //    {
        //        Debug.Log(ZoneName + " spawner " + i + " Transition In NOT Complete");
        //        TransitionIn = true;
        //    }
        //    else
        //    {
        //        Debug.Log("Min spawn interval for spawner " + i + " set to " + currentSpawners[i].timeBetweenSpawns.min);
        //        Debug.Log("Max spawn interval for spawner " + i + " set to " + currentSpawners[i].timeBetweenSpawns.max);
        //        Debug.Log("Max spawn scale for spawner " + i + " set to " + currentSpawners[i].scale.max);
        //        Debug.Log(ZoneName + " spawner " + i + " Transition In Complete");
        //    }
        //}

        //if (!TransitionIn)
        //{
        //    Debug.Log(ZoneName + " Transition In Complete");
        //    //return;
        //}

        //if (TimeSinceTransitionInStart > TransitionOutTime)
        //{
        //    TransitionIn = false;

        //    for (int i = 0; i < count; i++)
        //    {
        //        int currentSpawnerZoneArrayIndex = currentSpawners[i].ZoneArrayIndex;
        //        currentSpawners[i].timeBetweenSpawns.min = Spawners[currentSpawnerZoneArrayIndex].timeBetweenSpawns.min;
        //        Debug.Log("Min spawn interval for spawner " + i + " forced to " + currentSpawners[i].timeBetweenSpawns.min);
        //        currentSpawners[i].timeBetweenSpawns.max = Spawners[currentSpawnerZoneArrayIndex].timeBetweenSpawns.max;
        //        Debug.Log("Max spawn interval for spawner " + i + " forced to " + currentSpawners[i].timeBetweenSpawns.max);
        //        currentSpawners[i].scale.max = Spawners[currentSpawnerZoneArrayIndex].scale.max;
        //        Debug.Log("Max spawn scale for spawner " + i + " forced to " + currentSpawners[i].scale.max);
        //        Debug.Log(ZoneName + " spawner " + i + " Transition In Complete");
        //    }
        //    Debug.Log(ZoneName + " Transition In Forced");
        //}
    }

    private void TransitionOutStep()
    {
        //Debug.Log("Transition Out Step Run");

		//Increment TimeSinceTransitionOutStart
		TimeSinceTransitionOutStart = TimeSinceTransitionOutStart + (Time.deltaTime * 1f);

        //Set targets reached to false
        bool targetMinTimeReached = false;
       
        bool targetScaleReached = false;

        //Iterate through the currentSpawners list
        int count = new int();
        count = currentSpawners.Count;
        for (int i = 0; i < count; i++)
        {
            TransitionOut = true;
            targetMinTimeReached = false;
           
            targetScaleReached = false;

            int currentSpawnerZoneArrayIndex = currentSpawners[i].ZoneArrayIndex;
            float PreTransitionMinInterval = currentSpawners[i].PreTransitionMinInterval;
            float PreTransitionMaxInterval = currentSpawners[i].PreTransitionMaxInterval;
            float PreTransitionMaxScale = currentSpawners[i].PreTransitionMaxScale;

            //Evaluate TransitionIn curve
            float CurveEvaluation = Mathf.Pow(Spawners[currentSpawnerZoneArrayIndex].TransitionInCurve.Evaluate(TimeSinceTransitionOutStart / TransitionOutTime), 2.0f);
            //float CurveEvaluation = Spawners[currentSpawnerZoneArrayIndex].TransitionInCurve.Evaluate(TimeSinceTransitionOutStart / TransitionOutTime);

            float TargetMaxSpawnInterval = (Spawners[currentSpawnerZoneArrayIndex].timeBetweenSpawns.max * TransitionOutMultiplier);

            //If the current min and max *AREN'T* meant to be the same
            if (Spawners[currentSpawnerZoneArrayIndex].timeBetweenSpawns.min != Spawners[currentSpawnerZoneArrayIndex].timeBetweenSpawns.max)
            {
                //If the current min time has not yet reached the target interval
                if (currentSpawners[i].timeBetweenSpawns.min < TargetMaxSpawnInterval)
                {
                    //Lerp the two based on the animation curve

                    //Calculate Lerp value based on curve
                    float lerp = Mathf.Lerp(PreTransitionMinInterval, TargetMaxSpawnInterval, CurveEvaluation);

                    //Set to lerp
                    currentSpawners[i].timeBetweenSpawns.min = lerp;
                }

                //Debug.Log("Min spawn gap for spawner " + i + " = " + currentSpawners[i].timeBetweenSpawns.min);
                //Debug.Log("Target min spawn gap for spawner " + i + " = " + TargetMaxSpawnInterval);
                //Debug.Log("Distance to TargetMaxSpawnInterval for spawner " + i + " min is " + (TargetMaxSpawnInterval - currentSpawners[i].timeBetweenSpawns.min));

                //If the current max time has not yet reached the target interval
                if (currentSpawners[i].timeBetweenSpawns.max < TargetMaxSpawnInterval)
                {
                    //Lerp the two based on the animation curve

                    //Calculate Lerp value based on curve
                    float lerp = Mathf.Lerp(PreTransitionMaxInterval, TargetMaxSpawnInterval, CurveEvaluation);

                    //Set to lerp               
                    currentSpawners[i].timeBetweenSpawns.max = lerp;
                }

                //Debug.Log("Max spawn gap for spawner " + i + " = " + currentSpawners[i].timeBetweenSpawns.max);
                //Debug.Log("Target max spawn gap for spawner " + i + " = " + TargetMaxSpawnInterval);
                //Debug.Log("Distance to TargetMaxSpawnInterval for spawner " + i + " max is " + (TargetMaxSpawnInterval - currentSpawners[i].timeBetweenSpawns.max));
            }


            //If the spawner's min and max *AREN'T* supposed to be identical
            if (Spawners[currentSpawnerZoneArrayIndex].timeBetweenSpawns.max != Spawners[currentSpawnerZoneArrayIndex].timeBetweenSpawns.min)
            {
                //If the target min scale is not yet reached
                if (currentSpawners[i].scale.max > currentSpawners[i].scale.min)
                {
                    //Lerp the two based on the animation curve

                    //Calculate Lerp value based on curve
                    float lerp = Mathf.Lerp(PreTransitionMaxScale, Spawners[currentSpawnerZoneArrayIndex].scale.min, CurveEvaluation);

                    //Set to lerp
                    currentSpawners[i].scale.max = lerp;
                }
            }

            ////Debug.Log("Max scale for spawner " + i + " = " + currentSpawners[i].scale.max);
            ////Debug.Log("Target max scale for spawner " + i + " = " + currentSpawners[i].scale.min);
            ////Debug.Log("Distance to target scale for spawner " + i + " max is " + (currentSpawners[i].scale.max - currentSpawners[i].scale.min));




            if (currentSpawners[i].timeBetweenSpawns.min >= TargetMaxSpawnInterval)
            {
                //Debug.Log("Min time now at or above target");
                targetMinTimeReached = true;
            } else
            {
                targetMinTimeReached = false;
            }

            if (currentSpawners[i].scale.max <= currentSpawners[i].scale.min)
            {
                //Debug.Log("Max scale now at target");
                targetScaleReached = true;
            }
            else
            {
                targetScaleReached = false;
            }

            //If min time AND min scale are both at target, end the transition out, on the assumption this is the last object checked
            if (targetMinTimeReached && targetScaleReached)
            {
                TransitionOut = false;
                //Debug.Log(ZoneName + " Transition Out Completed Normally");

                //Debug.Log("Max spawn and min scale now both at targets");
                //Debug.Log("TransitionIn = " + TransitionIn);
                //Debug.Log("There are " + currentSpawners.Count + " spawners remaining");
                //Debug.Log("Returning spawner " + i + " to pool");
                currentSpawners[i].ReturnToPool();
                //Debug.Log("Removing " + currentSpawners[i].ToString() + " from list");
                //currentSpawners.RemoveAt(i);
                //Debug.Log(ZoneName + " Transition Out Complete");
            }
				
        }

        if (TimeSinceTransitionOutStart > TransitionOutTime)
        {

            //Debug.Log("TimeSinceTransitionOutStart exceeded TransitionOutTime");

            ////Iterate through the currentSpawners list
            //int overTimeCount = new int();
            //overTimeCount = currentSpawners.Count;
            //Debug.Log("There are  " + currentSpawners.Count + " remaining");
            //for (int i = 0; i < overTimeCount; i++)
            //{

            //    TransitionOut = false;
            //    targetTimeReached = true;
            //    targetScaleReached = true;

            //    Debug.Log("Returning spawner " + i + " to pool");
            //    currentSpawners[i].ReturnToPool();
            //}

            //Debug.Log(ZoneName + " Transition Out Should Be Forced");
        }
    }

    public float GetTransitionInTime()
    {
        if (TransitionInTime == 0f)
        {
            TransitionInTime = GenerateNextTransitionInTime();
        }
        return TransitionInTime;
    }

    public float GetTransitionOutTime()
    {
        if (TransitionOutTime == 0f)
        {
            TransitionOutTime = GenerateNextTransitionOutTime();
        }

        return TransitionOutTime;
    }

    public string GetZoneName()
    {
        return ZoneName;
    }

    public bool GetTransitionInProgress()
    {
        if (TransitionOut || TransitionIn)
        {
            return true;
        }
        return false;
    }

	public float GetZoneLength(){
		if (ZoneLength == 0f) {
			ZoneLength = ZoneLengthRange.RandomInRange;
		}

		return ZoneLength;
	}

    public float GenerateNextTransitionInTime()
    {
        TransitionInTime = TransitionLength.RandomInRange;
        return TransitionInTime;
    }

    public float GenerateNextTransitionOutTime()
    {
        TransitionOutTime = TransitionLength.RandomInRange;
        return TransitionOutTime;
    }

    public void RemoveRemainingSpawners()
    {
        //Debug.Log("Removing all remaining spawners");

        //Iterate through the currentSpawners list
        int overTimeCount = new int();
        overTimeCount = currentSpawners.Count;
        //Debug.Log("There are " + currentSpawners.Count + " spawners remaining");
        for (int i = 0; i < overTimeCount; i++)
        {

            TransitionOut = false;
           
            //Debug.Log("Returning spawner " + i + " to pool");
            currentSpawners[i].ReturnToPool();
        }
    }

    public void PreSpawn(float StartingWidth)
    {
        //Create a new temporary list to hold our BackdropObjectSpawners as we create them
        List<BackdropObjectSpawner> newSpawners = new List<BackdropObjectSpawner>();

        foreach (ObjectSpawner spawner in Spawners)
        {
            //Get a spawner from the pool based on the prefab
            BackdropObjectSpawner prefab = spawner.Spawner;
            BackdropObjectSpawner spawn = prefab.GetPooledInstance<BackdropObjectSpawner>();

            //Add it to our temporary list
            newSpawners.Add(spawn);
        }

        currentSpawners = newSpawners;

        //Iterate through the new currentSpawners structs        
        int count = new int();
        count = currentSpawners.Count;
        for (int i = 0; i < count; i++)
        {
            currentSpawners[i].transform.position = Spawners[i].position;
            currentSpawners[i].PreSpawn(StartingWidth);
        }
            
    }
}
