﻿using UnityEngine;
using System.Collections;

public class Object3AxisRotator : MonoBehaviour {

    [SerializeField]
    public Transform ObjectTransform;

    // Use this for initialization
    void OnEnable() {

        Vector3 newRotation = new Vector3(Random.Range(0f, 359f), Random.Range(0f, 359f), Random.Range(0f, 359f));
        
        ObjectTransform.Rotate(newRotation.x, newRotation.y, newRotation.z);
        
    }


}
