﻿using UnityEngine;

[System.Serializable]
public struct ObjectSpawner
{
    [SerializeField]
    public Vector3 position;
    [SerializeField]
    public BackdropObjectSpawner Spawner;
    [SerializeField]
    public FloatRange timeBetweenSpawns, scale;
    [SerializeField]
    public float currentSpeed;
    [SerializeField]
    public AnimationCurve TransitionInCurve;
    [SerializeField]
    public AnimationCurve TransitionOutCurve;
}
