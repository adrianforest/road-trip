﻿using UnityEngine;
using System.Collections;

public class ZoneCyclerScript : MonoBehaviour {

    public FloatRange ZoneLength;

    public Renderer GroundPlane;

    public BackdropZone[] Zones;

    public AnimationCurve GroundCurve;

    private BackdropZone currentZone;

    private BackdropZone nextZone;

    private int CurrentZoneIndex;
    private float TimeSinceZoneStart;
    private float NextZoneChangeTime;
    private float CurrentTransitionTime;

    //private bool TransitionInProgress;
    private bool TransitioningOut;
    private bool TransitioningIn;

    private Material OldGround;
    private Material NewGround;

    private float TimeSinceTransitionStart;
    private float OldZoneTransitionOutTime;
    private float NewZoneTransitionInTime;

    // Use this for initialization
    void Start () {

        CurrentZoneIndex = Random.Range(0, Zones.Length);
        BackdropZone prefab = Zones[CurrentZoneIndex];
        currentZone = prefab.GetPooledInstance<BackdropZone>();
        GroundPlane.material = currentZone.groundPlaneMaterial;
        currentZone.StartTransitionIn();
        TimeSinceTransitionStart = 0f;
        CurrentTransitionTime = currentZone.GetTransitionInTime();
        //Debug.Log("CurrentTransitionTime = " + CurrentTransitionTime);
        currentZone.GenerateNextTransitionOutTime();
        //TransitionInProgress = true;
        //Debug.Log("currentZone.GetTransitionInProgress() = " + currentZone.GetTransitionInProgress());
        OldGround = GroundPlane.material;
		NextZoneChangeTime = ZoneLength.RandomInRange;

    }
	
	// Update is called once per frame
	void Update () {

        //Increment TimeSinceZoneStart once per frame
        TimeSinceZoneStart = TimeSinceZoneStart + (Time.deltaTime * 1f);

        ////Check if the current zone has a transition in progress
        //TransitionInProgress = currentZone.GetTransitionInProgress();


        //If there's not already a transition in progress...
        if (!TransitioningOut && !TransitioningIn)
        {
            //Debug.Log("Transition is not in progress");

            //Check if it's time to transition to the next zone
            if (TimeSinceZoneStart >= NextZoneChangeTime)
            {
                Debug.Log("Transition Activated");
                //TransitionInProgress = true;

                //Tell the current zone to start transitioning out
                currentZone.StartTransitionOut();
                TransitioningOut = true;

                //Get the current zone's ground plane material so we can start lerping it to the new zone
                OldGround = currentZone.groundPlaneMaterial;

                //Select the next zone to transition into
                CurrentZoneIndex++;
                if (CurrentZoneIndex > (Zones.Length - 1))
                {
                    CurrentZoneIndex = 0;
                }
                BackdropZone prefab = Zones[CurrentZoneIndex];
                nextZone = prefab.GetPooledInstance<BackdropZone>();
                Debug.Log("nextzone selected = " + nextZone.ToString());
                

                //Get the new zone's ground plane material so we can start lerping to it
                NewGround = nextZone.groundPlaneMaterial;

                //Reset time since transition start
                TimeSinceTransitionStart = 0f;

                //Get the time it will take to perform the transition out
                OldZoneTransitionOutTime = currentZone.GetTransitionOutTime();

                //Get the time it will take to perform the next zone's transition in
                NewZoneTransitionInTime = nextZone.GetTransitionInTime();

                //Get the time it will take to perform the current transition in total
                CurrentTransitionTime = (OldZoneTransitionOutTime + NewZoneTransitionInTime);

            }
        } //end if (!TransitionInProgress) block


        //If a transition IS in progress
        else
        {
            //Debug.Log("currentZone.GetTransitionInProgress() = " + currentZone.GetTransitionInProgress());

            //Increment time since transition start
            TimeSinceTransitionStart = TimeSinceTransitionStart + (Time.deltaTime * 1f);
            //Debug.Log("TimeSinceTransitionStart is " + TimeSinceTransitionStart);

            ////If the OldGround doesn't match the NewGround
            if (GroundPlane.material.color != NewGround.color)
            {
                TransitionGround();
            }


            //Debug.Log("Debug Block Start");
            //Debug.Log("TimeSinceTransitionStart = " + TimeSinceTransitionStart);
            //Debug.Log("currentZone = " + currentZone.ToString());
            //Debug.Log("currentZone.GetTransitionOutTime() = " + currentZone.GetTransitionOutTime());
            //Debug.Log("CurrentTransitionTime = " + CurrentTransitionTime);

            //If we're transitioning out
            if (TransitioningOut)
            {
                
                //check if the transition is still in progress
                if (!currentZone.GetTransitionInProgress())
                {
                    Debug.Log("Removing current zone, starting transition to next zone");
                    //Return the current zone and all its spawners to the pool
                    currentZone.RemoveRemainingSpawners();
                    currentZone.ReturnToPool();
                    TransitioningOut = false;

                    //Start the transition to the new zone
                    nextZone.StartTransitionIn();
                    TransitioningIn = true;
                }
            }


            //Debug.Log("Checking if Transition is over. CurrentTransitionTime = " + CurrentTransitionTime + ". TimeSinceTransitionStart = " + TimeSinceTransitionStart);

            //If we're transitioning in
            if (TransitioningIn)
            {
                //check if the transition is still in progress
                if (!nextZone.GetTransitionInProgress())
                {
                    Debug.Log("Transition to next zone complete");

                    //Make the new zone the current zone
                    currentZone = nextZone;

                    //Reset time since zone start
                    TimeSinceZoneStart = 0f;

                    //Turn off Transition in progress
                    //TransitionInProgress = false;
                    TransitioningIn = false;

                    //Generate a new time until the next zone change
					NextZoneChangeTime = ZoneLength.RandomInRange;
                }
            }

            
        } //end if (TransitionInProgress) block

    }

    private void TransitionGround()
    {
        //Lerp OldGround towards NewGround 
        //Debug.Log("Lerping ground");

        Color NewColor = NewGround.color;

        //Create new TransitionMaterial
        //Material TransitionMaterial = new Material(GroundPlane.material);

        //Get lerp value to use to lerp TransitionMaterial towards NewGround
        //float lerp = Mathf.Lerp(Time.time, CurrentTransitionTime) / CurrentTransitionTime;
        //TransitionMaterial.Lerp(OldGround, NewGround, lerp);

        //float Percentage = (TimeSinceTransitionStart / CurrentTransitionTime);

        //Debug.Log("Percentage = " + (TimeSinceTransitionStart / CurrentTransitionTime));

        Color tempColor = new Color();

        //if (Percentage < 0.25f)
        //{
        //    tempColor = Color.Lerp(OldGround.color, NewColor, ((TimeSinceTransitionStart / 8f) / CurrentTransitionTime));
        //}

        //if (Percentage > 0.25f && Percentage < 0.50f)
        //{
        //    tempColor = Color.Lerp(OldGround.color, NewColor, ((TimeSinceTransitionStart / 4f) / CurrentTransitionTime));
        //}

        //if (Percentage > 0.50f && Percentage < 0.75f)
        //{
        //    tempColor = Color.Lerp(OldGround.color, NewColor, ((TimeSinceTransitionStart / 2f) / CurrentTransitionTime));
        //}

        //if (Percentage > 0.75f)
        //{
        //    tempColor = Color.Lerp(OldGround.color, NewColor, ((TimeSinceTransitionStart) / CurrentTransitionTime));
        //}

        tempColor = Color.Lerp(OldGround.color, NewColor, (Mathf.Pow(GroundCurve.Evaluate(TimeSinceTransitionStart / CurrentTransitionTime),2.0f)));
        

        GroundPlane.material.color = tempColor;
    }
    
}
