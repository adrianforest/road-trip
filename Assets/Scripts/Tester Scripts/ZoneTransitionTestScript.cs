﻿using UnityEngine;
using System.Collections;

public class ZoneTransitionTestScript : MonoBehaviour {

    public Renderer TestCube;

    public Renderer GroundPlane;

    public BackdropZone[] Zones;

    public AnimationCurve GroundCurve;

    public float GapBetweenZones;

    private BackdropZone currentZone;

    private BackdropZone nextZone;
    private bool ForcedNextZone = false;

    private float TimeSinceZoneStart;
    private float NextZoneChangeTime;
    private float CurrentTransitionTime;

    private bool TransitioningOut;
    private bool TransitioningIn;
    private bool TransitionOutComplete;

    private Material OldGround;
    private Material NewGround;
    [SerializeField]
    private float CurveMultiplier;
    [SerializeField]
    private float CurveMultiplierActual;

    private float TimeSinceTransitionStart;
    private float OldZoneTransitionOutTime;
    private float NewZoneTransitionInTime;

    // Use this for initialization
    void Start () {

        BackdropZone prefab = Zones[Random.Range(0, Zones.Length)];
        currentZone = prefab.GetPooledInstance<BackdropZone>();
        nextZone = currentZone;
        GroundPlane.material.color = currentZone.groundPlaneMaterial.color;
        TestCube.material.color = GroundPlane.material.color;
        currentZone.StartTransitionIn();
        TransitionOutComplete = true;
        TransitioningIn = true;
        TimeSinceTransitionStart = 0f;
        NewGround = currentZone.groundPlaneMaterial;
        CurveMultiplierActual = CurveMultiplier;
        CurrentTransitionTime = currentZone.GetTransitionInTime();
        currentZone.GenerateNextTransitionOutTime();
        OldGround = GroundPlane.material;
		NextZoneChangeTime = currentZone.GetZoneLength();

    }
	
	// Update is called once per frame
	void Update () {

        //Increment TimeSinceZoneStart once per frame
        TimeSinceZoneStart = TimeSinceZoneStart + (Time.deltaTime * 1f);

        ////Check if the current zone has a transition in progress
        //If there's not already a transition in progress...
        if (!TransitioningOut && !TransitioningIn)
        {
            if (GroundPlane.material.color != currentZone.groundPlaneMaterial.color)
            {
                //Debug.Log("Forcing GroundPlane colour change to current zone");
                GroundPlane.material.color = currentZone.groundPlaneMaterial.color;
            }

            //Check if it's time to transition to the next zone
            if (TimeSinceZoneStart >= NextZoneChangeTime)
            {
                StartTransition();
            }
        } //end if (!TransitionInProgress) block


        //If a transition IS in progress
        else
        {

            //Increment time since transition start
            TimeSinceTransitionStart = TimeSinceTransitionStart + (Time.deltaTime * 1f);

            ////If the OldGround doesn't match the NewGround
            if (GroundPlane.material.color != NewGround.color)
            {
                TransitionGround();
                if (GroundPlane.material.color == NewGround.color)
                {
                    //Debug.Log("Ground Transition Complete");
                }

                if (TransitioningIn && GroundPlane.material.color != NewGround.color)
                {
                    CurveMultiplierActual = CurveMultiplierActual * 1.00001f;
                    //Debug.Log("Ramping up Ground Transition");
                    //Debug.Log("CurveMultiplierActual is now " + CurveMultiplierActual);

                }
            }
            else
            {
                //GroundPlane.material.color = NewGround.color;
                //Debug.Log("Ground Transition Complete");
            }


            //If we're transitioning out
            if (TransitioningOut)
            {
                TransitionOutStep();
            }


            //If we're transitioning in
            if (TransitioningIn)
            {
                TransitionInStep();
            }

            
        } //end if (TransitionInProgress) block

    }

    private void TransitionGround()
    {
        //Debug.Log("Ground Transition In Progress");
        Color NewColor = NewGround.color;

        Color tempColor = new Color();

        CurrentTransitionTime = (OldZoneTransitionOutTime + NewZoneTransitionInTime + GapBetweenZones);

        CurveMultiplierActual = CurveMultiplierActual * 1.00001f;

        tempColor = Color.LerpUnclamped(OldGround.color, NewColor, (Mathf.Pow(GroundCurve.Evaluate(TimeSinceTransitionStart / CurrentTransitionTime), CurveMultiplierActual)));

        GroundPlane.material.color = tempColor;

        //Debug.Log("GroundPlane color is " + GroundPlane.material.color.ToString());
        //Debug.Log("Target color is " + NewGround.color.ToString());

        TestCube.material.color = GroundPlane.material.color;
    }

    private void StartTransition()
    {
        //Debug.Log("Transition Activated");

        //Tell the current zone to start transitioning out
        currentZone.StartTransitionOut();
        TransitioningOut = true;
        TransitionOutComplete = false;

        //Get the current zone's ground plane material so we can start lerping it to the new zone
        OldGround = currentZone.groundPlaneMaterial;

        CurveMultiplierActual = CurveMultiplier;

        if (!ForcedNextZone)
        {
            //Select the next zone to transition into
            BackdropZone prefab = Zones[Random.Range(0, Zones.Length)];
            nextZone = prefab.GetPooledInstance<BackdropZone>();
            //Debug.Log("nextzone selected = " + nextZone.ToString());
        }
        


        //Get the new zone's ground plane material so we can start lerping to it
        NewGround = nextZone.groundPlaneMaterial;

        //Reset time since transition start
        TimeSinceTransitionStart = 0f;

        //Get the time it will take to perform the transition out
        OldZoneTransitionOutTime = currentZone.GetTransitionOutTime();

        //Get the time it will take to perform the next zone's transition in
        NewZoneTransitionInTime = nextZone.GetTransitionInTime();

        //Get the time it will take to perform the current transition in total
        CurrentTransitionTime = (OldZoneTransitionOutTime + NewZoneTransitionInTime + GapBetweenZones);
    }

    private void TransitionOutStep()
    {
        //check if the transition is still in progress
        if (!currentZone.GetTransitionInProgress())
        {
            

            if (TimeSinceTransitionStart >= OldZoneTransitionOutTime && !TransitionOutComplete)
            {
                TransitionOutComplete = true;
                //Debug.Log("Removing current zone");
                //Return the current zone and all its spawners to the pool
                currentZone.RemoveRemainingSpawners();
                currentZone.ReturnToPool();
            }
            

            if (TimeSinceTransitionStart >= OldZoneTransitionOutTime && (TimeSinceTransitionStart >= (OldZoneTransitionOutTime + GapBetweenZones)))
            {
                //Debug.Log("Zone gap started");
            }

            if (TimeSinceTransitionStart >= (OldZoneTransitionOutTime + GapBetweenZones))
            {
                //Debug.Log("Starting new zone transition in");
                //Start the transition to the new zone
                nextZone.StartTransitionIn();
                TransitioningOut = false;
                TransitioningIn = true;
            }

        }

    }

    private void TransitionInStep()
    {
        //check if the transition is still in progress
        if (!nextZone.GetTransitionInProgress() && !currentZone.GetTransitionInProgress())
        {
            //Debug.Log("Transition to next zone complete");

            if (GroundPlane.material.color != NewGround.color)
            {
                Debug.Log("Forcing GroundPlane colour change to new zone");
                GroundPlane.material.color = NewGround.color;
            }

                //Make the new zone the current zone
                currentZone = nextZone;

            //Reset time since zone start
            TimeSinceZoneStart = 0f;

            //Reset time since transition start
            TimeSinceTransitionStart = 0f;

            //Turn off Transition in progress
            TransitioningIn = false;

            //Generate a new time until the next zone change
            NextZoneChangeTime = currentZone.GetZoneLength();
        }
    }

    public void DebugSetNextZone(string ZoneName)
    {
        //Debug.Log("Requesting " + ZoneName);

        ForcedNextZone = true;
        //Iterate through the zones array        
        int count = new int();
        count = Zones.Length;
        for (int i = 0; i < count; i++)
        {
            //Debug.Log("Comparing " + Zones[i].GetZoneName() + " to " + ZoneName);
            if (Zones[i].GetZoneName() == ZoneName)
            {
                BackdropZone prefab = Zones[i];
                nextZone = prefab.GetPooledInstance<BackdropZone>();
                //Debug.Log("Next zone set to " + Zones[i].GetZoneName());
                return;
            }
        }
        //Debug.Log("ZONE NOT FOUND");
    }

    public void DebugStartTransition()
    {
        StartTransition();
    }

}
