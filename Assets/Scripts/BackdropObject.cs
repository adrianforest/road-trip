﻿using UnityEngine;

public class BackdropObject : PooledObject {

    [SerializeField]
    private float currentSpeed;


    void FixedUpdate()
    {
        transform.Translate(currentSpeed, 0, 0);
    }

    void OnTriggerEnter(Collider enteredCollider)
    {
        //If the object has entered the kill zone
        if (enteredCollider.CompareTag("Kill Zone"))
        {
            //Return it to the pool
            ReturnToPool();
        }
    }

    public void setSpeed(float newSpeed)
    {
        currentSpeed = newSpeed;
    }
}
