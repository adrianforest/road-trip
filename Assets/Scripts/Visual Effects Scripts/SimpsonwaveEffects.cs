﻿using UnityEngine;
using System.Collections;
using Kino;
using UnityStandardAssets.ImageEffects;

[RequireComponent (typeof (AnalogGlitch))]
[RequireComponent (typeof (DigitalGlitch))]
[RequireComponent (typeof (EdgeDetection))]

public class SimpsonwaveEffects : MonoBehaviour {

    //Selective Effects State
    private bool EdgeDetect;
    
    //Effect Strength Settings

    private float CurrentEffectStrength;

    private const float GlitchOff = 0f;
    [SerializeField]
    private float VeryLow;
    [SerializeField]
    private float Low;
    [SerializeField]
    private float Normal;
    [SerializeField]
    private float High;
    [SerializeField]
    private float VeryHigh;

    //Effect Strength index constants
    private const int GLoff = 0;
    private const int GLvlow = 1;
    private const int GLlow = 2;
    private const int GLnorm = 3;
    private const int GLhigh = 4;
    private const int GLvhigh = 5;

    //Image effect components
    private AnalogGlitch AnalogComponent;
	private DigitalGlitch DigitalComponent;
	private EdgeDetection EdgeComponent;

    public FloatRange GlitchChangeInterval;
    private float timeSinceLastGlitchChange = 0f;

	[Header("Analog Glitch Settings")]
	public FloatRange JitterRange;
	public AnimationCurve JitterFrequency;
	public FloatRange VerticalJumpRange;
	public AnimationCurve VerticalJumpFrequency;
	public FloatRange HorizontalShakeRange;
	public AnimationCurve HorizontalShakeFrequency;
	public FloatRange ColorDriftRange;
	public AnimationCurve ColorDriftFrequency;

	[Header("Digital Glitch Settings")]
	public FloatRange IntensityRange;
	public AnimationCurve IntensityFrequency;


	//EdgeDetection settings for Simpsons-style outlines
	private static EdgeDetection.EdgeDetectMode mode = EdgeDetection.EdgeDetectMode.RobertsCrossDepthNormals;
	private static float sensitivityDepth = 0.18f;
	private static float sensitivityNormals = 0.75f;
	private static float sampleDist = 1.5f;
	private static float edgesOnly = 0.0f;
	private static Color edgesOnlyBgColor = Color.white;



	// Use this for initialization
	void Start () {
		
        InitialiseEffects();

		//Set EdgeDetection for Simpsons-style outlines
		EdgeComponent.mode = mode;
		EdgeComponent.sensitivityDepth = sensitivityDepth;
		EdgeComponent.sensitivityNormals = sensitivityNormals;
		EdgeComponent.sampleDist = sampleDist;
		EdgeComponent.edgesOnly = edgesOnly;
		EdgeComponent.edgesOnlyBgColor = edgesOnlyBgColor;

        timeSinceLastGlitchChange = 0f;


    }
	
	// Update is called once per frame
	void Update () {

        timeSinceLastGlitchChange = timeSinceLastGlitchChange + (1.0f * Time.deltaTime);

        //Set initial Analog settings
        if(timeSinceLastGlitchChange > GlitchChangeInterval.RandomInRange)
        {
            AnalogComponent.scanLineJitter = (JitterRange.RandomInRange * JitterFrequency.Evaluate(Random.value)) * CurrentEffectStrength;
            timeSinceLastGlitchChange = 0f;
        }

        if (timeSinceLastGlitchChange > GlitchChangeInterval.RandomInRange)
        {
            AnalogComponent.verticalJump = (VerticalJumpRange.RandomInRange * VerticalJumpFrequency.Evaluate(Random.value)) * CurrentEffectStrength;
            timeSinceLastGlitchChange = 0f;
        }

        if (timeSinceLastGlitchChange > GlitchChangeInterval.RandomInRange)
        {
            AnalogComponent.horizontalShake = (HorizontalShakeRange.RandomInRange * HorizontalShakeFrequency.Evaluate(Random.value)) * CurrentEffectStrength;
            timeSinceLastGlitchChange = 0f;
        }

        if (timeSinceLastGlitchChange > GlitchChangeInterval.RandomInRange)
        {
            AnalogComponent.colorDrift = (ColorDriftRange.RandomInRange * ColorDriftFrequency.Evaluate(Random.value)) * CurrentEffectStrength;
            timeSinceLastGlitchChange = 0f;
        }



        //Set initial Digital settings
        if (timeSinceLastGlitchChange > GlitchChangeInterval.RandomInRange)
        {
            DigitalComponent.intensity = (IntensityRange.RandomInRange * IntensityFrequency.Evaluate(Random.value)) * CurrentEffectStrength;
            timeSinceLastGlitchChange = 0f;
        }
        
	
	}

    public void SetEffectStrength(int index)
    {
        switch (index)
        {
            case GLoff:
                CurrentEffectStrength = GlitchOff;
                break;

            case GLvlow:
                CurrentEffectStrength = VeryLow;
                break;

            case GLlow:
                CurrentEffectStrength = Low;
                break;

            case GLnorm:
                CurrentEffectStrength = Normal;
                break;

            case GLhigh:
                CurrentEffectStrength = High;
                break;

            case GLvhigh:
                CurrentEffectStrength = VeryHigh;
                break;
        }

        InitialiseEffects();
    }

    private void InitialiseEffects()
    {

        //Get the Glitch Components
        AnalogComponent = GetComponent<AnalogGlitch>();
        DigitalComponent = GetComponent<DigitalGlitch>();
        EdgeComponent = GetComponent<EdgeDetection>();

        //Set initial Analog settings
        AnalogComponent.scanLineJitter = (JitterRange.RandomInRange * JitterFrequency.Evaluate(Random.value)) * CurrentEffectStrength;
        AnalogComponent.verticalJump = (VerticalJumpRange.RandomInRange * VerticalJumpFrequency.Evaluate(Random.value)) * CurrentEffectStrength;
        AnalogComponent.horizontalShake = (HorizontalShakeRange.RandomInRange * HorizontalShakeFrequency.Evaluate(Random.value)) * CurrentEffectStrength;
        AnalogComponent.colorDrift = (ColorDriftRange.RandomInRange * ColorDriftFrequency.Evaluate(Random.value)) * CurrentEffectStrength;

        //Set initial Digital settings
        DigitalComponent.intensity = (IntensityRange.RandomInRange * IntensityFrequency.Evaluate(Random.value)) * CurrentEffectStrength;
    }

    //public int GetEffectStrength()
    //{
    //    switch (CurrentEffectStrength)
    //    {
    //        case GlitchOff:
    //            return GLoff;
    //            break;

    //        case VeryLow:
    //            return GLvlow;
    //            break;

    //        case Low:
    //            return GLlow;
    //            break;

    //        case Normal:
    //            return GLnorm;
    //            break;

    //        case High:
    //            return GLhigh;
    //            break;

    //        case VeryHigh:
    //            return GLvhigh;
    //            break;

    //        default:
    //            return null;
    //    }
    //}
}
