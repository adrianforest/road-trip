﻿using UnityEngine;
using System.Collections;

public class CheatScript : MonoBehaviour {

    [SerializeField]
    private VisualsManagerScript VM;

    [SerializeField]
    private GameObject Car;

    [SerializeField]
    private Vector3 CarOnPos;

    [SerializeField]
    private Vector3 CarOffPos;

    [SerializeField]
    private float CarSpeed;

    [SerializeField]
    private GameObject TitleCanvas;

    [SerializeField]
    private ConversationGenerator ConvoGen;

    [SerializeField]
    private GameObject CarTitle;

    private bool GlitchOn = false;

    private bool CarOn = true;

    private bool titleOn = false;

    private bool carTitleOn = false;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        #region Car Update

        if(CarOn && Car.transform.position != CarOnPos)
        {
            Car.transform.position = new Vector3(Car.transform.position.x - (CarSpeed * Time.deltaTime), Car.transform.position.y);
            if (Car.transform.position.x < CarOnPos.x)
            {
                Car.transform.position = CarOnPos;
            }
        }

        if (!CarOn && Car.transform.position != CarOffPos)
        {
            Car.transform.position = new Vector3(Car.transform.position.x + (CarSpeed * Time.deltaTime), Car.transform.position.y);
            if (Car.transform.position.x > CarOffPos.x)
            {
                Car.transform.position = CarOffPos;
            }
        }

        #endregion

        if (Input.GetKeyDown(KeyCode.Keypad1))
        {
            ToggleGlitch();
        }

        if (Input.GetKeyDown(KeyCode.Keypad2))
        {
            ToggleCar();
        }

        if (Input.GetKeyDown(KeyCode.Keypad3))
        {
            ToggleTitle();
        }

        if (Input.GetKeyDown(KeyCode.Keypad4))
        {
            ToggleConvo();
        }

        if (Input.GetKeyDown(KeyCode.Keypad5))
        {
            ToggleCarTitle();
        }

    }

    private void ToggleGlitch()
    {
        if (GlitchOn)
        {
            VM.DisableSimpsonwave();
            GlitchOn = false;
            ConvoGen.setSimpsonwaveStatus(GlitchOn);
        }
        else
        {
            VM.EnableSimpsonwave();
            GlitchOn = true;
            ConvoGen.setSimpsonwaveStatus(GlitchOn);
        }
    }

    private void ToggleCar()
    {
        CarOn = !CarOn;
        if (!CarOn)
        {
            ConvoGen.DeactivateConversationMode();
        }
    }

    private void ToggleTitle()
    {
        TitleCanvas.SetActive(!titleOn);
        titleOn = !titleOn;
    }

    private void ToggleConvo()
    {
        if (CarOn && Car.transform.position == CarOnPos)
        {
            if (ConvoGen.GetConversationMode())
            {
                ConvoGen.DeactivateConversationMode();
            }
            else
            {
                ConvoGen.ActivateConversationMode();
            }
        }
        
    }

    private void ToggleCarTitle()
    {
        CarTitle.SetActive(!carTitleOn);
        carTitleOn = !carTitleOn;
    }
}
