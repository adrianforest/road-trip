﻿using UnityEngine;
using System.Collections;

public class ObjectCardinalRotatorScript : MonoBehaviour {

    [SerializeField]
    public GameObject ObjectTransform;

    // Use this for initialization
    void OnEnable()
    {
        int newY = Random.Range(0, 3);

        switch (newY)
        {
            case 0:
                ObjectTransform.transform.Rotate(ObjectTransform.transform.rotation.x, 0f, ObjectTransform.transform.rotation.z);
                break;
            case 1:
                ObjectTransform.transform.Rotate(ObjectTransform.transform.rotation.x, 90f, ObjectTransform.transform.rotation.z);
                break;
            case 2:
                ObjectTransform.transform.Rotate(ObjectTransform.transform.rotation.x, 180f, ObjectTransform.transform.rotation.z);
                break;
            case 3:
                ObjectTransform.transform.Rotate(ObjectTransform.transform.rotation.x, 270f, ObjectTransform.transform.rotation.z);
                break;
        }

    }


}
