﻿using UnityEngine;

public class ObjectHorizontalRotator : MonoBehaviour {

    [SerializeField]
    public Transform ObjectTransform;

	void OnEnable()
    {
        float newY = Random.Range(0f, 359f);
        //Debug.Log("Generated value = " + newY);
        ObjectTransform.Rotate(ObjectTransform.rotation.x, newY, ObjectTransform.rotation.z);
        //this.transform.localRotation.Set(this.transform.localRotation.x, newY, this.transform.localRotation.z, this.transform.localRotation.w);
        //Debug.Log("Rotation set to " + ObjectTransform.rotation.y);
    }
}
