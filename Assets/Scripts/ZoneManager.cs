﻿using UnityEngine;
using System.Collections;

public class ZoneManager : MonoBehaviour {

    public BackdropZone Backdrop1;

    public BackdropZone Backdrop2;

    public BackdropZone StartingZone;

    public FloatRange ZoneLength;

    public FloatRange TransitionLength;

    public Renderer GroundPlane;

    public BackdropZone[] Zones;

    private BackdropZone currentZone;

    private BackdropZone nextZone;

    private float TimeSinceZoneStart;
    private float NextZoneChangeTime;
    private float CurrentTransitionTime;
    private bool TransitionInProgress;
    private Material OldGround;
    private Material NewGround;
    private float TimeSinceTransitionStart;

    // Use this for initialization
    void Start () {

        Backdrop1 = StartingZone;
        currentZone = Backdrop1;

        GroundPlane.material = currentZone.groundPlaneMaterial;
        currentZone.StartTransitionIn();
        NextZoneChangeTime = ZoneLength.RandomInRange;
        TimeSinceZoneStart = 0f;
        TransitionInProgress = true;


    }
	
	// Update is called once per frame
	void Update () {
        TimeSinceZoneStart = TimeSinceZoneStart + (Time.deltaTime * 1f);

        if (!TransitionInProgress)
        {
            Debug.Log("Transition is not in progress");
            if (TimeSinceZoneStart >= NextZoneChangeTime)
            {
                Debug.Log("Transition Activated");
                TimeSinceTransitionStart = 0f;
                CurrentTransitionTime = TransitionLength.RandomInRange;

                currentZone.StartTransitionOut();
                OldGround = currentZone.groundPlaneMaterial;

                Backdrop2 = Zones[1];
                nextZone = Backdrop2;
                nextZone.StartTransitionIn();
                NewGround = currentZone.groundPlaneMaterial;

                TransitionInProgress = true;
            }
        } else
        {
            TimeSinceTransitionStart = TimeSinceTransitionStart + (Time.deltaTime * 1f);
            if (OldGround != NewGround)
            {
                Debug.Log("Lerping ground");
                float lerp = Mathf.PingPong(Time.time, CurrentTransitionTime) / CurrentTransitionTime;
                GroundPlane.material.Lerp(OldGround, NewGround, lerp);
            }
        }

        if (TimeSinceTransitionStart >= CurrentTransitionTime)
        {
            Backdrop1 = Backdrop2;
            Backdrop2 = null;
        }
        

	}
}
