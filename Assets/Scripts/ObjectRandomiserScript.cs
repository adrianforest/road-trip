﻿using UnityEngine;
using System.Collections;

public class ObjectRandomiserScript : MonoBehaviour {

    [SerializeField]
    private Transform RandomObjectPosition;
    [SerializeField]
    private BackdropObject RandomObject;
    [SerializeField]
    private BackdropObject[] SetOfObjects;


    

	// Run every time the object is enabled
	void OnEnable() {


        BackdropObject prefab = SetOfObjects[Random.Range(0, SetOfObjects.Length)];
        
        RandomObject = prefab.GetPooledInstance<BackdropObject>();
        RandomObject.transform.localScale = this.transform.localScale;
        RandomObject.transform.localRotation = this.transform.localRotation;
        
    }

    void OnDisable()
    {
        Destroy(RandomObject);
    }
}
