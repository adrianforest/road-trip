﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CarPassengers : ScriptableObject
{

    #region Seat position constants
    //Seat position constants
    private const int driver = 0;
    private const int front = 1;
    private const int left = 2;
    private const int right = 3;
    #endregion



    //The number of seats in the car
    private int numSeats = 4;

    //The occupied state of each of the four seats
    private bool[] OccupiedSeats;

    //The index of the head of each passenger
    private int[] PassengerHeads;

    //The index of the head of each passenger
    private bool[] InitialisedPassengers;

    //The index of the player's seat
    private int PlayerPosition;

    //Array of all currently-occupied seats
    private int[] CurrentPassengers;


    //Default Constructor
    public CarPassengers()
    {
        
        //Initialise OccupiedSeats
        OccupiedSeats = new bool[numSeats];

        //The front two positions are always true;
        OccupiedSeats[driver] = true;
        OccupiedSeats[front] = true;

        //The rear sets are initialised as false to be set later
        OccupiedSeats[left] = false;
        OccupiedSeats[right] = false;


        //Initialise CurrentPassengers
        refreshCurrentPassengers();

        //Initialise PassengerHeads
        PassengerHeads = new int[numSeats];

        //Initialise InitialisedPassengers
        InitialisedPassengers = new bool[numSeats];

        //Set all of InitialisedPassengers to false;
        for (int i = 0; i < InitialisedPassengers.Length; i++)
        {
            InitialisedPassengers[i] = false;
        }

    }



    #region Getters

    //Returns whether the seat at a given position is occupied
    public bool isOccupied(int passenger) { return OccupiedSeats[passenger]; }

    //Returns whether the seat at a given position is initialised
    public bool isInitialised(int passenger) { return InitialisedPassengers[passenger]; }

    //Returns the index of the PassengerHead at a given position
    public int passengerHeadIndex(int passenger) { return PassengerHeads[passenger]; }

    //Returns the number of total seating positions in the car
    public int getNumSeats() { return numSeats; }

    //Returns the current player position
    public int getPlayerPosition() { return PlayerPosition; }

    //Returns the index of a random passenger
    public int GetRandomPassenger()
    {
        int randomPassenger = Random.Range(0, CurrentPassengers.Length);
        return CurrentPassengers[randomPassenger];
    }

    #endregion


    //Sets a passenger initialisation status, returns a bool indicating if the seat is occupied
    public bool setPassengerInitialised(int passenger, bool status)
    {
        //if the passenger index is outside the range of passengers, return false
        if (passenger > InitialisedPassengers.Length || passenger < 0)
        {
            return false;
        }

        //Set the passenger head index
        InitialisedPassengers[passenger] = status;

        //If the seat is occupied return true, else return false
        return isOccupied(passenger);
    }


    #region Set Seat Occupied Status Functions
    //Sets the Occupied status of a rear passenger based on a string indicating a side, and returns a bool indicating if this was successful
    public bool setRearPassenger(string side, bool status)
    {

        if (side == "right" || side == "Right")
        {
           //Debug.Log("Setting rear right passenger to " + status);
            OccupiedSeats[right] = status;
            refreshCurrentPassengers();
            return true;
        }

        if (side == "left" || side == "Left")
        {
           //Debug.Log("Setting rear left passenger to " + status);
            OccupiedSeats[left] = status;
            refreshCurrentPassengers();
            return true;
        }

        return false;
    }

    //Sets the Occupied status of a rear passenger based on an int indicating a side, and returns a bool indicating if this was successful
    public bool setRearPassenger(int side, bool status)
    {

        if (side == right)
        {
           //Debug.Log("Setting rear right passenger to " + status);
            OccupiedSeats[right] = status;
            refreshCurrentPassengers();
            return true;
        }

        if (side == left)
        {
           //Debug.Log("Setting rear left passenger to " + status);
            OccupiedSeats[left] = status;
            refreshCurrentPassengers();
            return true;
        }

        return false;
    }

    //Sets the Occupied status of the front passenger seat
    public void setFrontPassenger(bool status)
    {
        OccupiedSeats[front] = status;
        refreshCurrentPassengers();
    }

    //Sets the Occupied status of the driver seat
    public void setDriver(bool status)
    {
        OccupiedSeats[driver] = status;
        refreshCurrentPassengers();
    }
    #endregion


    //Sets a passenger head index, returns a bool indicating if the seat is occupied
    public bool setPassengerHead(int passenger, int passengerhead)
    {
        //if the passenger index is outside the range of passengers, return false
        if (passenger > PassengerHeads.Length || passenger < 0)
        {
            return false;
        }

        //Set the passenger head index
        PassengerHeads[passenger] = passengerhead;

        //If the seat is occupied return true, else return false
        return isOccupied(passenger);
    }
    
    

    //Sets the player's position to a random current passenger position
    public void SetPlayerPosition()
    {
        PlayerPosition = GetRandomPassenger();
    }



    //Re-size and re-index Current passengers array
    private void refreshCurrentPassengers()
    {
        //Determine the new size of the array
        int passengerCount = 0;
        for (int i = 0; i<OccupiedSeats.Length; i++)
        {
            if (OccupiedSeats[i])
            {
                passengerCount++;
            }
        }

        //Resize the array
        CurrentPassengers = new int[passengerCount];

        //Repopulate the array
        int j = 0;
        for (int i = 0; i < OccupiedSeats.Length; i++)
        {
            if (OccupiedSeats[i])
            {
                CurrentPassengers[j] = i;
                j++;
            }
        }
        
    }
}
