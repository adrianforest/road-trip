﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Text;
using UnityEngine.UI;
using Random = UnityEngine.Random;

#region Internal Classes


//This holds the Quads where passenger heads are displayed
[System.Serializable]
public class PassengerHeads
{
    public GameObject Passenger0OutlineQuad;
    public GameObject[] PassengerHeadQuads;
}

//This holds the GameObjects that encapsulate the rear passengers' bodies
[System.Serializable]
public class RearPassengers
{
    public GameObject[] RearPassengerBodies;
}

//This holds all the assets related to the car, so they can be activated, deactivated and modified
[System.Serializable]
public class CarAssets
{
    //The car windows, so we can make them partially transparent
    public MeshRenderer carWindows;

    //The car windows, so we can activate/de-activate them, and make them partially transparent
    public GameObject carWindowObject;

    //The Transparent version of the car window material
    public Material TransparentWindowMaterial;

    //The car interior so we can activate/deactivate it
    public GameObject carInterior;

    //The car windows' original material so we can restore it on de-activation
    public Material nonTransparentWindowMaterial;
}

//This holds all information about passenger configuration
[System.Serializable]
public class PassengerElements
{
    [Range(0.0f, 1.0f)]
    public float ChanceOfRearPassenger;

    [Range(0.0f, 1.0f)]
    public float ChanceOfTwoRearPassengers;

    [Range(0.0f, 1.0f)]
    public float RearBalance = 0.5f;

    //The passenger head quads, used to display Passenger head images
    public PassengerHeads PassengerHeads;

    //The passenger speech and text elements
    public PassengerSpeechBubbles SpeechBubbles;

    //The rear passengers' bodies, so we can activate and deactivate them
    public RearPassengers RearPassengers;

    //The pre-configured passengers who could be in the car
    public RoadTripper[] Passengers;

    //The pre-configured Simpsons-style passengers who could be in the car
    public RoadTripper[] SimpsonPassengers;
}


#endregion



public class ConversationGenerator : MonoBehaviour {

    #region Seat position constants
    //Seat position constants
    private const int driver = 0;
    private const int front = 1;
    private const int left = 2;
    private const int right = 3;
    #endregion

    #region Inspector Variables

    //This indicates whether conversation mode is currently active
    [SerializeField]
	private bool ConversationModeOn;

    //This holds all the assets related to the car, so they can be activated, deactivated and modified
    [SerializeField]
    private CarAssets CarElements;

    //This allows the player to partake in the conversation
    [SerializeField]
    private PlayerConversation PlayerConvo;

    //This holds all configuration information about conversation patterns
    [SerializeField]
	private ConversationConfigurator ConversationConfig;

    //This holds all information about passenger configuration
    [SerializeField]
    private PassengerElements PassengerElements;

    //This indicates whether conversation mode is currently active
    [SerializeField]
    private bool CartoonWaveModeOn;

    #endregion


    #region Internal Variables
    //This holds all the information about which seats are occupied, and which passenger head is in them
    private CarPassengers CurrentPassengers;

    //This gives access to all the necessary functions to allow the script to parse and display emojis from the RawImage
    private EmojiFunctions emojiAssets;

    //This holds all the information about the current status of the conversation
    private ConversationStatus ConvoStatus;

    //Whether Conversation Mode has been initialised and seats populated
    private bool ConvoModeInitialised = false;

    //Whether the emoji system has been initialised
    private bool emojiInitialised = false;

    #endregion


    // Use this for initialization
    void Start () {

        //Turn on Windows
        CarElements.carWindowObject.SetActive(true);
        DeactivateWindowTransparency();

        //Deactivate rear passenger bodies
        for (int i = 0; i < PassengerElements.RearPassengers.RearPassengerBodies.Length; i++)
        {
            PassengerElements.RearPassengers.RearPassengerBodies[i].SetActive(false);
        }

        emojiAssets = ScriptableObject.CreateInstance<EmojiFunctions>();

        CurrentPassengers = ScriptableObject.CreateInstance<CarPassengers>();

        ConvoStatus = ScriptableObject.CreateInstance<ConversationStatus>();

        //Turn on Conversation Mode
        if (ConversationModeOn)
		{
			//Activate Conversation Mode
			ActivateConversationMode();

			//Reset the ConvoStatus.TimeSinceLastEmoji
            ConvoStatus.TimeSinceLastEmoji = ConversationConfig.ConversationGap;

        }

        //Check if Player Input is on
        if (PlayerConvo.PlayerInputStatus())
        {
            //Assign the player a position
            CurrentPassengers.SetPlayerPosition();
        }


    }
	
	// Update is called once per frame
	void Update () {

        if (!emojiInitialised)
        {
            //This sends the text file to ParseEmojiInfo to be put into the Dictionary
            emojiAssets.ParseEmojiInfo(ConversationConfig.textAsset.text);
            emojiInitialised = true;
        }

       //Debug.Log("ConversationModeOn = " + ConversationModeOn + " ConvoStatus.WaitingOnPlayer = " + ConvoStatus.WaitingOnPlayer);
		if (ConversationModeOn && !ConvoStatus.WaitingOnPlayer)
		{
            ConversationStep(ConvoStatus.ConversationInProgress);   
        }
        			
	}


    #region Conversation Mode Functions

    public bool GetConvoInitialised()
    {
        return ConvoModeInitialised;
    }

    //Initialise all variables required for Conversation mode
    public void InitialiseConversationMode()
	{
       //Debug.Log("Initialising Conversation Mode");

        //Scale the rawImage to the current Canvas scale
        ConversationConfig.rawImageToClone.rectTransform.localScale = (ConversationConfig.rawImageToClone.rectTransform.localScale * ConversationConfig.ConversationCanvas.scaleFactor);

        //Check to see if we have a rear passenger
        float randrear = Random.value;
       //Debug.Log("Rear passenger roll is " + randrear);
        if (randrear < PassengerElements.ChanceOfRearPassenger)
        {
            //Check to see if we have TWO rear passengers
            float randtwo = Random.value;
           //Debug.Log("Two rear roll is " + randtwo);
            if (randtwo < PassengerElements.ChanceOfTwoRearPassengers)
            {
                //Update CurrentPassengers
                CurrentPassengers.setRearPassenger(left, true);
                CurrentPassengers.setRearPassenger(right, true);
            }
            else
            {
                //Check to see which side the rear passenger is on
                float randside = Random.value;
               //Debug.Log("Rear side roll is " + randside);
                if (randside > PassengerElements.RearBalance)
                {
                    //Update CurrentPassengers
                    CurrentPassengers.setRearPassenger(left, true);
                    CurrentPassengers.setRearPassenger(right, false);

                }
                else
                {
                    //Update CurrentPassengers
                    CurrentPassengers.setRearPassenger(right, true);
                    CurrentPassengers.setRearPassenger(left, false);
                }
            }
        }
        else
        {
            //Update CurrentPassengers
            CurrentPassengers.setRearPassenger(left, false);
            CurrentPassengers.setRearPassenger(right, false);
        }


        ConvoModeInitialised = true;
    }

    //Activate Conversation Mode
    public void ActivateConversationMode()
    {

       //Debug.Log("Activating Conversation Mode");

        //Enable the conversation canvas
		ConversationConfig.ConversationCanvas.enabled = true;

        //Initialise Conversation Mode, if it isn't already
        if (!ConvoModeInitialised) { InitialiseConversationMode(); }

        //setShowPassengers(true);

        //Reset ConvoStatus values
        ConvoStatus.TimeSinceLastEmoji = ConversationConfig.ConversationGap;
        ConvoStatus.ConversationInProgress = false;
        ConversationConfig.CurrentConvoChance = ConversationConfig.StartingConversationChance;

        //Set ConversationModeOn to true
        ConversationModeOn = true;
    }

    //Deactivate Conversation Mode
    public void DeactivateConversationMode()
    {

       //Debug.Log("Deactivating Conversation Mode");

        //Clear All emoji
        ClearAllEmoji();

        //Clear all speech bubbles
        PassengerElements.SpeechBubbles.ClearAllSpeech();

        //Disable the conversation canvas
        ConversationConfig.ConversationCanvas.enabled = false;

        //setShowPassengers(false);

        //Set ConversationModeOn to false
        ConversationModeOn = false;

    }

    //Sets Player Input to the given bool
    public void SetPlayerInput(bool inputStatus) { PlayerConvo.SetPlayerInputStatus(inputStatus); }

    //Returns a bool indicating player input status
    public bool GetPlayerInput() { return PlayerConvo.PlayerInputStatus(); }

    //Activates or Deactivates ConversationMode, based on a given bool
    public void SetConversationMode(bool inputStatus)
    {
       //Debug.Log("Setting Conversation Mode to " + inputStatus);

        if (inputStatus != ConversationModeOn)
        {
           //Debug.Log("inputStatus != ConversationModeOn, changing Conversation Mode status");
            if (inputStatus)
            {
                ActivateConversationMode();
            }
            else
            {
                DeactivateConversationMode();
           }
        }

    }

    //Returns a bool indicating Conversation Mode status
    public bool GetConversationMode() { return ConversationModeOn; }

    #endregion


    #region Passenger Functions

    public bool getShowPassengers()
    {
        if(CarElements.carInterior.activeInHierarchy)
        {
            return true;
        }

        return false;
    }

    public void setShowPassengers(bool newStatus)
    {

        //Initialise Conversation Mode, if it isn't already
        if (!ConvoModeInitialised) { InitialiseConversationMode(); }

        if (newStatus)
        {
            //Activate car window transparency
            ActivateWindowTransparency();

            //Activate Front Passengers
            ActivatePassenger(driver);
            ActivatePassenger(front);

            //Activate rear passengers, if they exist
            if (CurrentPassengers.isOccupied(left))
            {
                //Debug.Log("Activating Left passenger");
                ActivatePassenger(left);
            }

            if (CurrentPassengers.isOccupied(right))
            {
                //Debug.Log("Activating Right passenger");
                ActivatePassenger(right);
            }
        }
        else
        {
            DeactivateConversationMode();

            //Deactivate car window transparency
            DeactivateWindowTransparency();

            //Deactivate Front Passengers
            DeactivatePassenger(driver);
            DeactivatePassenger(front);

            //Deactivate rear passengers, if they exist
            if (CurrentPassengers.isOccupied(left))
            {
                //Debug.Log("Deactivating Left passenger");
                DeactivatePassenger(left);
            }

            if (CurrentPassengers.isOccupied(right))
            {
                //Debug.Log("Deactivating Right passenger");
                DeactivatePassenger(right);
            }
        }

        
    }

    //Picks a passenger head from the array of pre-configured heads, and sets the Quad at a given seat position to that head
    private void InitialisePassenger(int passenger, RoadTripper[] PassengerHeads)
	{
      //Debug.Log("Initialising Passenger index " + passenger);

		//Get a new int to determine which of the pre-configured heads we'll use
		int newPassengerIndex = UnityEngine.Random.Range(0, PassengerHeads.Length);

       //Debug.Log("Selected Passenger head " + newPassengerIndex);

        //If the passenger is in position 0, set up the outline
        #region Position1HeadOutline
        if (passenger == 0) {

            //Debug.Log("Initialising Passenger head outline for passenger " + passenger);
            //Create a new GameObject to refer to the Passenger0OutlineQuad
            GameObject passengerObjectoutline = PassengerElements.PassengerHeads.Passenger0OutlineQuad;

			//Activate the Quad
			passengerObjectoutline.SetActive(true);

			//Get the material for the outline
			passengerObjectoutline.GetComponent<Renderer>().material = PassengerHeads[newPassengerIndex].GetHeadOutline();
            
            //Get the Transform for the outline
            Transform passengerObjectOutlineHead = PassengerHeads[newPassengerIndex].GetOutlineTransform();

            //Debug.Log("Setting Quad transforms for head outline for passenger " + passenger);
            //Set the Outline's Transform to the proper properties 
            passengerObjectoutline.transform.localPosition = passengerObjectOutlineHead.position;
            passengerObjectoutline.transform.rotation = passengerObjectOutlineHead.rotation;
            passengerObjectoutline.transform.localScale = passengerObjectOutlineHead.localScale;

            //Deactivate the Quad
            passengerObjectoutline.SetActive(false);

		}
		#endregion

        //Create temp GameObjects for passenger and passenger bodies (for rear passengers)
		GameObject passengerObject = new GameObject();

        //If the passenger is in the rear, activate the appropriate rear passenger body
        if (passenger == left || passenger == right)
        {
            //Debug.Log("Setting body active for rear passenger body " + (passenger-3));
            PassengerElements.RearPassengers.RearPassengerBodies[passenger - 2].SetActive(true);
        }

        //Set passengerObject to the quad corresponding to the passenger position
        passengerObject = PassengerElements.PassengerHeads.PassengerHeadQuads[passenger];

        //Set the Quad to Active
		passengerObject.SetActive(true);

        //Debug.Log("Setting head image for passenger " + passenger);
        //Set the Quad to the image for the pre-configured head we're using
        passengerObject.GetComponent<Renderer>().material = PassengerHeads[newPassengerIndex].GetHeadImage();

        //Create a HeadTransform to hold the transform data for the pre-configured head
		Transform passengerObjectHead = PassengerHeads[newPassengerIndex].GetPositionTransform(passenger);

        //Debug.Log("Setting Quad transforms for passenger " + passenger);
        //Set the Quad's transform to the position, rotation and scale from the pre-configured head
        passengerObject.transform.localPosition = passengerObjectHead.position;
		passengerObject.transform.rotation = passengerObjectHead.rotation;
        passengerObject.transform.localScale = passengerObjectHead.localScale;

		//Deactivate the Quad
		passengerObject.SetActive(false);

        //If the passenger is in the rear
        if (passenger == left || passenger == right)
        {
            //Deactivate the passenger body
            PassengerElements.RearPassengers.RearPassengerBodies[passenger - 2].SetActive(false);
        }
        
        //Set current passenger to Initialised
        CurrentPassengers.setPassengerInitialised(passenger, true);

        //Set current passenger head
        CurrentPassengers.setPassengerHead(passenger, newPassengerIndex);

        
    }

    //Activates the passenger at the given index
    private void ActivatePassenger(int passenger)
    {

     //Debug.Log("Activating passenger " + passenger);

        if (!CurrentPassengers.isInitialised(passenger))
        {
            if (CartoonWaveModeOn)
            {
                InitialisePassenger(passenger, PassengerElements.SimpsonPassengers);
            }
            else
            {
                InitialisePassenger(passenger, PassengerElements.Passengers);
            }
        }

        if(passenger == driver)
        {
            PassengerElements.PassengerHeads.Passenger0OutlineQuad.SetActive(true);
        }

        //If the passenger is in the rear
        if (passenger == left || passenger == right)
        {
           //Debug.Log("Activating body for passenger " + passenger);
            //activate the appropriate rear passenger body
            PassengerElements.RearPassengers.RearPassengerBodies[passenger - 2].SetActive(true);
            //update CurrentPassengers
            CurrentPassengers.setRearPassenger(passenger, true);
        }

        PassengerElements.PassengerHeads.PassengerHeadQuads[passenger].SetActive(true);

    }

    private void DeactivatePassenger(int passenger)
    {
       //Debug.Log("Deactivating passenger " + passenger);
        //Deactivate passenger head quad
        PassengerElements.PassengerHeads.PassengerHeadQuads[passenger].SetActive(false);

        //If the passenger is in the rear
        if (passenger == 3 || passenger == 4)
        {
            //Deactivate the passenger body
            PassengerElements.RearPassengers.RearPassengerBodies[passenger - 2].SetActive(false);

            //update CurrentPassengers
            //CurrentPassengers.setRearPassenger(passenger, false);
        }


    }

    public void setSimpsonwaveStatus(bool newStatus)
    {
        if(newStatus == CartoonWaveModeOn)
        {
            return;
        }

        CartoonWaveModeOn = newStatus;

        bool passengersVisible = getShowPassengers();

        for (int i = 0; i < CurrentPassengers.getNumSeats(); i++)
        {
            if (CurrentPassengers.isOccupied(i))
            {
                
                if (CartoonWaveModeOn)
                {
                    InitialisePassenger(i, PassengerElements.SimpsonPassengers);
                }
                else
                {
                    InitialisePassenger(i, PassengerElements.Passengers);
                }
                
                if (ConversationModeOn)
                {
                   ActivatePassenger(i);
                }
                
            }
        }

        setShowPassengers(passengersVisible);
    }

    public bool getSimpsonwaveStatus()
    {
        return CartoonWaveModeOn;
    }

    #endregion


    #region Car Window Functions
    //Swaps the car window material with a transparent version
    private void ActivateWindowTransparency()
    {
        //CarElements.nonTransparentWindowMaterial = CarElements.carWindows.material;
        CarElements.carWindows.material = CarElements.TransparentWindowMaterial;
        CarElements.carInterior.SetActive(true);
    }

    //Resets the car window material to its original non-transparent version
    private void DeactivateWindowTransparency()
    {
        CarElements.carWindows.material = CarElements.nonTransparentWindowMaterial;
        CarElements.carInterior.SetActive(false);
    }
    #endregion


    #region Emoji Display Functions
    //Clears all emoji on the screen
    private void ClearAllEmoji()
	{
		for (int j = ConvoStatus.displayedEmojis.Count-1; j > -1; j--)
		{
			Destroy(ConvoStatus.displayedEmojis[j]);
			ConvoStatus.displayedEmojis.RemoveAt(j);
		}
	}

    private void PassengerEmote(int passenger)
    {
       //Debug.Log("Making passenger " + passenger + " speak a random emoji");

        PassengerElements.SpeechBubbles.ActivatePassengerSpeech(passenger);
        ConvoStatus.LastEmojiUsed = ShowRandomEmoji(PassengerElements.SpeechBubbles.GetPassengerTextField(passenger));
    }

    private void PassengerEmote(int passenger, int lastemoji)
    {
       //Debug.Log("Making passenger " + passenger + " speak a related emoji");

        PassengerElements.SpeechBubbles.ActivatePassengerSpeech(passenger);
        ConvoStatus.LastEmojiUsed = ShowRelatedEmoji(PassengerElements.SpeechBubbles.GetPassengerTextField(passenger), lastemoji);
    }

    private void PassengerEmoteSpecific(int passenger, int specificEmoji)
    {
       //Debug.Log("Making passenger " + passenger + " speak emoji index " + specificEmoji);

        PassengerElements.SpeechBubbles.ActivatePassengerSpeech(passenger);
        ConvoStatus.LastEmojiUsed = ShowSpecificEmoji(PassengerElements.SpeechBubbles.GetPassengerTextField(passenger), specificEmoji);
    }

    //This takes a Text field and displays a random emoji on it
    public int ShowRandomEmoji(Text emojiText)
    {
        //Creates a new copy of the RawImage GameObject to display the emoji
        GameObject newRawImage = GameObject.Instantiate(this.ConversationConfig.rawImageToClone.gameObject);
        ConvoStatus.displayedEmojis.Add(newRawImage);

        //Sets the RawImage's parent to the text field where the emoji will appear
        newRawImage.transform.SetParent(emojiText.transform);

        //Creates a Vector3 to hold the position of the RawImage
        Vector3 imagePos = new Vector3(0f, 0f, 0f);

        //Sets the new RawImage's local position (within its parent Text field
        newRawImage.transform.localPosition = imagePos;

        //Create a new RawImage based on the RawImage created above
        RawImage ri = newRawImage.GetComponent<RawImage>();

        //Get a random emoji from the Dictionary
        int randomEmoji = UnityEngine.Random.Range(0, emojiAssets.emojiRectValues.Length);

        ri.uvRect = emojiAssets.emojiRectValues[randomEmoji];

        return randomEmoji;

    }

    public int ShowRelatedEmoji(Text emojiText, int lastEmoji)
    {
        //Creates a new copy of the RawImage GameObject to display the emoji
        GameObject newRawImage = GameObject.Instantiate(this.ConversationConfig.rawImageToClone.gameObject);
        ConvoStatus.displayedEmojis.Add(newRawImage);

        //Sets the RawImage's parent to the text field where the emoji will appear
        newRawImage.transform.SetParent(emojiText.transform);

        //Creates a Vector3 to hold the position of the RawImage
        Vector3 imagePos = new Vector3(0f, 0f, 0f);

        //Sets the new RawImage's local position (within its parent Text field
        newRawImage.transform.localPosition = imagePos;

        //Create a new RawImage based on the RawImage created above
        RawImage ri = newRawImage.GetComponent<RawImage>();


        //Determine the emoji range to draw on
        int minEmoji = lastEmoji - (int)(ConversationConfig.ConversationDriftRange / 2);
        int maxEmoji = lastEmoji + (int)(ConversationConfig.ConversationDriftRange / 2);

        //Modulate our range to keep it within the Dictionary range
        if(minEmoji < 0)
        {
            minEmoji = emojiAssets.emojiRectValues.Length + minEmoji;
        }

        if (maxEmoji > emojiAssets.emojiRectValues.Length)
        {
            maxEmoji = maxEmoji -emojiAssets.emojiRectValues.Length;
        }

        if (minEmoji > maxEmoji)
        {
            int tempInt = minEmoji;
            minEmoji = maxEmoji;
            maxEmoji = tempInt;
        }

        //Get a related emoji from the Dictionary
        int relatedEmoji = UnityEngine.Random.Range(minEmoji, maxEmoji);

        ri.uvRect =emojiAssets.emojiRectValues[relatedEmoji];

        return relatedEmoji;

    }

    private int ShowSpecificEmoji(Text emojiText, int specificEmoji)
    {
        //Creates a new copy of the RawImage GameObject to display the emoji
        GameObject newRawImage = GameObject.Instantiate(this.ConversationConfig.rawImageToClone.gameObject);
        ConvoStatus.displayedEmojis.Add(newRawImage);

        //Sets the RawImage's parent to the text field where the emoji will appear
        newRawImage.transform.SetParent(emojiText.transform);

        //Creates a Vector3 to hold the position of the RawImage
        Vector3 imagePos = new Vector3(0f, 0f, 0f);

        //Sets the new RawImage's local position (within its parent Text field
        newRawImage.transform.localPosition = imagePos;

        //Create a new RawImage based on the RawImage created above
        RawImage ri = newRawImage.GetComponent<RawImage>();

        ri.uvRect =emojiAssets.emojiRectValues[specificEmoji];

        return specificEmoji;
    }
    #endregion


    #region Player Response Functions
    
    //Activate player input mode
    public void ActivatePlayerInputMode()
    {
        PlayerConvo.SetPlayerInputStatus(true);
    }

    //Deactivate player input mode
    public void DeactivatePlayerInputMode()
    {
        PlayerConvo.SetPlayerInputStatus(false);
    }

    //Takes the player's response and records it as the last emoji used
    public void PlayerResponse(int selectedEmoji)
    {
        
        //Make the passenger in the player position emote the selected emoji
        PassengerEmoteSpecific(CurrentPassengers.getPlayerPosition(), selectedEmoji);

        //Reset time since last emote
        ConvoStatus.TimeSinceLastEmoji = 0f;

        //No longer waiting for a response
        ConvoStatus.WaitingOnPlayer = false;
    }

    //Called when the player takes too long to respond in a conversation
    public void PlayerResponseTimeout()
    {
        ConvoStatus.WaitingOnPlayer = false;
    }
    #endregion


    #region Conversation Functions

    private void ConversationStep(bool status)
    {
        if (!status)
        {
            //Check to see if it's been long enough since the last conversation
            if (ConvoStatus.TimeSinceLastEmoji > ConversationConfig.ConversationGap)
            {
                //Debug.Log("ConvoStatus.TimeSinceLastEmoji > ConversationConfig.ConversationGap");
                //Check to see if a conversation will start
                if (UnityEngine.Random.value < ConversationConfig.CurrentConvoChance)
                {
                    StartConversation();
                }
                else
                {
                    //Debug.Log("Increasing ConvoChance");
                    ConversationConfig.CurrentConvoChance = ConversationConfig.CurrentConvoChance + ConversationConfig.ChanceIncreaseRate;
                    //Debug.Log("Current Conversation Chance = " + ConversationConfig.CurrentConvoChance);
                }
            }
            else
            {
                //Debug.Log("Increasing ConvoStatus.TimeSinceLastEmoji");
                //Increment time since last emote
                ConvoStatus.TimeSinceLastEmoji = ConvoStatus.TimeSinceLastEmoji + (1f * Time.deltaTime);
            }
        }
        else
        {
            //Increment time since last emote
            ConvoStatus.TimeSinceLastEmoji = ConvoStatus.TimeSinceLastEmoji + (1f * Time.deltaTime);

            //Check if it's time for someone else to speak
            if (ConvoStatus.TimeSinceLastEmoji > ConversationConfig.ConversationInterval)
            {
                //Debug.Log("Time For Another Emoji");
                //Deactivate the last speaker's bubble and clear their emoji
                PassengerElements.SpeechBubbles.DeactivatePassengerSpeech(ConvoStatus.LastSpeaker);
                ClearAllEmoji();

                //Check to see if the conversation will continue
                if (UnityEngine.Random.value < ConversationConfig.ConversationContinueChance)
                {
                    ContinueConversation();
                }
                else
                {
                    //Debug.Log("Conversation Ended");
                    ConvoStatus.ConversationInProgress = false;
                    ConversationConfig.CurrentConvoChance = ConversationConfig.StartingConversationChance;
                    //Debug.Log("Next Conversation Chance = " + ConversationConfig.CurrentConvoChance);

                }
            }
            else
            {
                //Debug.Log("Not Yet Time For Another Emoji");
            }
        }


    }
    
    private void StartConversation()
    {
        //Debug.Log("Conversation Started");
        //Start the conversation
        ConvoStatus.ConversationInProgress = true;

        //Select the first speaker
        ConvoStatus.LastSpeaker = CurrentPassengers.GetRandomPassenger();
        //Debug.Log("Selected Speaker = " + ConvoStatus.LastSpeaker);

        //If the speaker selected is the player, and player input is turned on...

        //Debug.Log("PlayerConvo.PlayerInputStatus = " + PlayerConvo.PlayerInputStatus() + " Player Speaking = " + (ConvoStatus.LastSpeaker == CurrentPassengers.getPlayerPosition()));
        if (ConvoStatus.LastSpeaker == CurrentPassengers.getPlayerPosition() && PlayerConvo.PlayerInputStatus())
        {
            //Debug.Log("Player Choice Started");
            //Show them a choice of emojis
            PlayerConvo.OfferPlayerInput(ConvoStatus.LastEmojiUsed);

            //Wait for them to respond
            ConvoStatus.WaitingOnPlayer = true;
        }
        else
        {
            //Debug.Log("Non-Player Speaking");
            //Make them speak
            PassengerEmote(ConvoStatus.LastSpeaker);
            //Debug.Log("ConvoStatus.LastEmojiUsed = " + ConvoStatus.LastEmojiUsed);

            //Reset time since last emote
            ConvoStatus.TimeSinceLastEmoji = 0f;
        }
    }

    private void ContinueConversation()
    {
        //Debug.Log("Conversation Continues");

        //Select the next speaker
        ConvoStatus.LastSpeaker = CurrentPassengers.GetRandomPassenger();

        //Debug.Log("Selected Speaker = " + ConvoStatus.LastSpeaker);

        //If the speaker selected is the player
        //Debug.Log("PlayerConvo.PlayerInputStatus = " + PlayerConvo.PlayerInputStatus() + " Player Speaking = " + (ConvoStatus.LastSpeaker == CurrentPassengers.getPlayerPosition()));
        if (ConvoStatus.LastSpeaker == CurrentPassengers.getPlayerPosition() && PlayerConvo.PlayerInputStatus())
        {
            //Debug.Log("Player Choice Started");
            //Show them a choice of emojis
            PlayerConvo.OfferPlayerInput(ConvoStatus.LastEmojiUsed);

            //Wait for them to respond
            ConvoStatus.WaitingOnPlayer = true;
        }
        else
        {

            //Debug.Log("Non-Player Speaking");

            //Check if this will be a non-sequitur or related
            if (UnityEngine.Random.value < ConversationConfig.ConversationNonSequiturChance)
            {

                //Debug.Log("Non-Sequitur");
                //Make them speak a random emoji
                PassengerEmote(ConvoStatus.LastSpeaker);
                //Debug.Log("ConvoStatus.LastEmojiUsed = " + ConvoStatus.LastEmojiUsed);
            }
            else
            {
                //Debug.Log("Related");
                PassengerEmote(ConvoStatus.LastSpeaker, ConvoStatus.LastEmojiUsed);
                //Debug.Log("ConvoStatus.LastEmojiUsed = " + ConvoStatus.LastEmojiUsed);
            }
            //Reset time since last emote
            ConvoStatus.TimeSinceLastEmoji = 0f;
        }
    }


    #endregion

}
