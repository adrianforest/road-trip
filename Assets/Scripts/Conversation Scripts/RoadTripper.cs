﻿using UnityEngine;
using System.Collections;

// Custom serializable class
[System.Serializable]
public class HeadTransform
{
    public Vector3 Position;
    public Vector3 Rotation;
    public Vector3 Scale;
}

public class RoadTripper : MonoBehaviour {

    //Image to use as a Material
    [SerializeField]
    private Material HeadImage;
    //Image to use as an Outline Material when in the driver position
    [SerializeField]
    private Material HeadImageOutline;

    //Transform for the outline position
    [SerializeField]
    private Transform OutlinePosition;

    //Transforms for correct image alignment at each seating position
    [SerializeField]
    private Transform[] HeadPositions;

    

    //Is this a human-shaped head?
    [SerializeField]
    private bool Human;

	// Use this for initialization
	//void Start () {
	
	//}
	
	// Update is called once per frame
	//void Update () {
	
	//}

    public Material GetHeadImage()
    {
        return HeadImage;
    }

    public Material GetHeadOutline()
    {
        return HeadImageOutline;
    }

    public Transform GetPositionTransform(int pos)
    {
        return HeadPositions[pos];

    }

    public Transform GetOutlineTransform()
    {
        return OutlinePosition;

    }

    public bool isHuman()
    {
        return Human;
    }
}
