﻿using UnityEngine;
using System.Collections.Generic;

public class ConversationStatus : ScriptableObject {

    //The time since a speaker last emoted.
    public float TimeSinceLastEmoji = 0f;

    //Whether or not a conversation is currently in-progress
    public bool ConversationInProgress = false;

    //Which passenger last spoke based on their seat position
    public int LastSpeaker = 0;

    //The last emoji spoken, based on its index in the emoji Dictionary
    public int LastEmojiUsed = 0;

    //Are we waiting for a response from the player?
    public bool WaitingOnPlayer = false;

    //List of all currently-displayed emojis
    public List<GameObject> displayedEmojis;

    public ConversationStatus()
    {
        //Initialise Lists
        displayedEmojis = new List<GameObject>();
    }
}
