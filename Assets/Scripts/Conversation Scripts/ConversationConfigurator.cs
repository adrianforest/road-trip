﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Text;
using UnityEngine.UI;

public class ConversationConfigurator : MonoBehaviour {

    //The current chance of a conversation starting
    public float CurrentConvoChance;

    //The chance of a conversation at the start
    [Range(0.0f, 1.0f)]
    public float StartingConversationChance;

    //The rate at which the chance of a conversation increases
    [Range(0.0f, 1.0f)]
    public float ChanceIncreaseRate;

    //The chance a conversation continues
    [Range(0.0f, 1.0f)]
    public float ConversationContinueChance;

    //The time between statements in a conversation
    public float ConversationInterval;

    //The distance a conversation can drift to "related" (close in the emoji index) topics
    public int ConversationDriftRange;

    //The chance of a conversation going on a non-sequitur tangent
    [Range(0.0f, 1.0f)]
    public float ConversationNonSequiturChance;

    //The minimum interval between different conversations
    public int ConversationGap;

    //The text file that contains the co-ordinates in the atlas for each Unicode emoji
    public TextAsset textAsset;

    //The canvas that shows the Conversation
    public Canvas ConversationCanvas;

    //The RawImage that holds the emoji atlas texture.
    //This gets cloned and used to display the emoji by setting it to the offset corresponding to that emoji
    public RawImage rawImageToClone;

    // Use this for initialization
    void Start () {

        CurrentConvoChance = StartingConversationChance;

    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
