﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Text;
using UnityEngine.UI;

public class EmojiTesterScript : MonoBehaviour {

	//The text file that contains the co-ordinates in the atlas for each Unicode emoji
	[SerializeField]
	private TextAsset textAsset;

    //The text field to replace with emojis
    [SerializeField]
    private Text emojiTextField;

	//The RawImage that holds the emoji atlas texture.
	//This gets cloned and used to display the emoji by setting it to the offset corresponding to that emoji
	[SerializeField]
	private RawImage rawImageToClone;

	//This dictionary holds the positions of the each emoji on the emoji atlas texture, indexed by unicode String
	private Dictionary<string, Rect> emojiRects = new Dictionary<string, Rect>();

	//These arrays will hold all the Keys and values from the emoji dictionary so we can pull a random one easily.
	private string[] emojiRectKeys;
    private Rect[] emojiRectValues;

	//This is the character used to create a space for the emoji
	//private static char emSpace = '\u2001';

    List<GameObject> displayedEmojis;

    // Use this for initialization
    void Start () {

        //Initialise displayedEmojis
        displayedEmojis = new List<GameObject>();

        //This sends the text file to ParseEmojiInfo to be put into the Dictionary
        this.ParseEmojiInfo(this.textAsset.text);



	}

    //	// Update is called once per frame
    //	void Update () {
    //	
    //	}

    public void ShowRandomEmoji()
    {
        //Creates a new copy of the RawImage GameObject to display the emoji
        GameObject newRawImage = GameObject.Instantiate(this.rawImageToClone.gameObject);
        displayedEmojis.Add(newRawImage);

        //Sets the RawImage's parent to the text field where the emoji will appear
        newRawImage.transform.SetParent(emojiTextField.transform);

        //Creates a Vector3 to hold the position of the RawImage
        Vector3 imagePos = new Vector3(0f, 0f, 0f);

        //Sets the new RawImage's local position (within its parent Text field
        newRawImage.transform.localPosition = imagePos;

        //Create a new RawImage based on the RawImage created above
        RawImage ri = newRawImage.GetComponent<RawImage>();
        

        //Get a random emoji from the Dictionary
        int randomEmoji = UnityEngine.Random.Range(0, emojiRectValues.Length);

        ri.uvRect = emojiRectValues[randomEmoji];

    }


    public void ShowRandomEmoji(Text emojiText)
	{
		//Creates a new copy of the RawImage GameObject to display the emoji
		GameObject newRawImage = GameObject.Instantiate(this.rawImageToClone.gameObject);
        displayedEmojis.Add(newRawImage);

        //Sets the RawImage's parent to the text field where the emoji will appear
        newRawImage.transform.SetParent(emojiText.transform);

		//Creates a Vector3 to hold the position of the RawImage
		Vector3 imagePos = new Vector3(0f, 0f, 0f);

		//Sets the new RawImage's local position (within its parent Text field
		newRawImage.transform.localPosition = imagePos;

		//Create a new RawImage based on the RawImage created above
		RawImage ri = newRawImage.GetComponent<RawImage>();

        //Get a random emoji from the Dictionary
        int randomEmoji = UnityEngine.Random.Range(0, emojiRectValues.Length);

		ri.uvRect = emojiRectValues[randomEmoji];

	}

    public void ClearAllEmoji()
    {
        for (int j = displayedEmojis.Count-1; j > -1; j--)
        {
            Destroy(displayedEmojis[j]);
            displayedEmojis.RemoveAt(j);
        }
    }


	//This function takes a string and converts it to a UTF-16 encoded string
	// This used to convert the unicode hexadecimal values from the emoji atlas' index file into decimal values stored as strings
	private static string GetConvertedString(string inputString)
	{
		//Create an array to hold parts of a string split at the '-' character
		string[] converted = inputString.Split('-');

		//Iterate through the array of strings
		for (int j = 0; j < converted.Length; j++)
		{
			//Convert the string from a hexadecimal value to a UTF-16 encoded string
			converted[j] = char.ConvertFromUtf32(Convert.ToInt32(converted[j], 16));
		}
		//returns a string consisting of all the strings joined together with empty characters
		return string.Join(string.Empty, converted);
	}

	//This function takes the text asset and uses it to add the atlas positions of all the emojis to emojiRects
	//This is used to populate the dictionary with the atlas positions of all emojis,
	//indexed by their Unicode value (as a decimal string)
	private void ParseEmojiInfo(string inputString)
	{
		//Reading the strings
		using (StringReader reader = new StringReader(inputString))
		{
			//Create a string to hold the line we read
			string line = reader.ReadLine();

			//Iterate through each line of the text asset
			while (line != null && line.Length > 1)
			{
				// We add each emoji to emojiRects

				//Create an array that holds the line, split into different strings, separated by spaces
				string[] split = line.Split(' ');

				//Create a new float x based on the string at [1]
				float x = float.Parse(split[1], System.Globalization.CultureInfo.InvariantCulture);

				//Create a new float y based on the string at [2]
				float y = float.Parse(split[2], System.Globalization.CultureInfo.InvariantCulture);

				//Create a new float width based on the string at [3]
				float width = float.Parse(split[3], System.Globalization.CultureInfo.InvariantCulture);

				//Create a new float height based on the string at [4]
				float height = float.Parse(split[4], System.Globalization.CultureInfo.InvariantCulture);

				//Set the emojiRect element at the index of the Unicode value (as a decimal string)
				//to a Rect holding the position and size on the atlas of the corresponding emoji
				this.emojiRects[GetConvertedString(split[0])] = new Rect(x, y, width, height);

				line = reader.ReadLine();
			}
		}

        //Recreate emojiRectKeys and emojiRectValues to be the size of emojiRects
        emojiRectKeys = new string[emojiRects.Count];
        emojiRectValues = new Rect[emojiRects.Count];

        //Read the Keys and Values from emojiRects into emojiRectKeys and emojiRectValues
        emojiRects.Keys.CopyTo(emojiRectKeys, 0);
        emojiRects.Values.CopyTo(emojiRectValues, 0);


    }
}