﻿using UnityEngine;
using UnityEngine.UI;
//using System.Collections;
using System.Collections.Generic;

public class PlayerConversation : MonoBehaviour {


    #region Inspector Variables

    //Whether Player Input Mode is on
    [SerializeField]
    private bool PlayerInputOn;

    //How long to wait for the player to respond before the conversation moves on without them
    [SerializeField]
    private float PlayerResponseWaitTime;

    [SerializeField]
    [Range(0.0f, 1.0f)]
    private float NonsequiturResponsePercentage;

    //The Conversation Generator in the scene
    [SerializeField]
    private ConversationGenerator ConvoGenenerator;

    //The panel that shows the player's response options
    [SerializeField]
    private GameObject PlayerInputPanel;

    //The array that holds all the response buttons shown to the player
    [SerializeField]
    private Button[] responseButtons;

    //The bar showing how much time remains for the player to respond
    [SerializeField]
    private Image PlayerResponseBar;

    #endregion

    //The starting width of the player response bar, so it can be restored to default after the player responds
    private float PlayerResponseBarStartingWidth;

    //Whether the script is waiting for the player to respond
    private bool WaitingOnPlayerResponse = false;

    //How much of the player's response time is remaining
    private float RemainingWaitTime = 0f;

    //This dictionary holds the emoji shown on each button, indexed by the button's index in the responseButtons Array
    private Dictionary<int, int> buttonEmoji = new Dictionary<int, int>();

    

    //private bool TestEmojisShown = false;

    // Use this for initialization
    void Start () {

        PlayerResponseBarStartingWidth = PlayerResponseBar.rectTransform.sizeDelta.x;
        //Debug.Log("PlayerResponseBarStartingWidth = " + PlayerResponseBarStartingWidth);

        PlayerInputPanel.SetActive(false);

        //RemainingWaitTime = PlayerResponseWaitTime;

    }
	
	// Update is called once per frame
	void Update () {

        //Check if waiting on player response
        if (WaitingOnPlayerResponse)
        {
            //Decrement remaining wait time
            RemainingWaitTime = RemainingWaitTime - (1f * Time.deltaTime);

            //Check if there's still time remaining
            if(RemainingWaitTime > 0f)
            {
                //Calculate the percentage of the total wait time remaining
                float WaitPercentage = (RemainingWaitTime / PlayerResponseWaitTime);
                float NewResponseBarWidth = PlayerResponseBarStartingWidth * WaitPercentage;

                //Set the response bar to reflect the percentage of response time remaining
                PlayerResponseBar.rectTransform.sizeDelta = new Vector2(NewResponseBarWidth, PlayerResponseBar.rectTransform.sizeDelta.y);
            }
            else
            {
                PlayerResponseTimeOut();
            }
        }
	
	}

    //return the current status of PlayerInputOn
    public bool PlayerInputStatus()
    {
        return PlayerInputOn;
    }

    //Set PlayerInputOn to a new bool
    public void SetPlayerInputStatus(bool newPlayerInputStatus)
    {
            PlayerInputOn = newPlayerInputStatus;
    }

    //Show the PlayerInputPanel and offer them a set of responses to choose from
    public void OfferPlayerInput(int lastEmoji)
    {
        //Activate the PlayerInputPanel
        PlayerInputPanel.SetActive(true);

        //Reset the PlayerResponseBar's width to its starting width
        PlayerResponseBar.rectTransform.sizeDelta = new Vector2(PlayerResponseBarStartingWidth, PlayerResponseBar.rectTransform.sizeDelta.y);


        //////////////////////////////////////////////////////////
        //Determine which buttons will show non-sequitur responses
        //////////////////////////////////////////////////////////
        //Create an array of bools to reflect which buttons are non-sequiturs
        bool[] isButtonNonSequitur = new bool[responseButtons.Length];

        #region SelectNonSequiturs

        //Initialise the isButtonNonSequitur array's values to false
        for (int i = 0; i < isButtonNonSequitur.Length; i++)
        {
            isButtonNonSequitur[i] = false;
        }

        //Calculate number of non-sequitur responses to show
        int NumberOfNonSequiturs = (int)(responseButtons.Length * NonsequiturResponsePercentage);

        
        //Create a list of possible non-sequiturs
        List<int> PossibleNonSequiturs = new List<int>();

        //Add an int for each button in the responseButtons array
        for (int i = 0; i < responseButtons.Length; i++)
        {
            PossibleNonSequiturs.Add(i);
        }

        //Create a list of actual non-sequiturs
        List<int> NonSequiturs = new List<int>();

        //For each non-sequitur required
        for (int i = 0; i < NumberOfNonSequiturs; i++)
        {
            //Get one of the possible non-sequiturs
            int NonSeq = Random.Range(0, PossibleNonSequiturs.Count - 1);
            
            //add it to the list of actual non-sequiturs
            NonSequiturs.Add(NonSeq);
            
            //remove it from the list of possible non-sequiturs
            PossibleNonSequiturs.RemoveAt(NonSeq);
        }


        //Set the relevant values in the isButtonNonSequitur array to true
        foreach (int item in NonSequiturs)
        {
            isButtonNonSequitur[item] = true;
        }
        #endregion

        /////////////////////////////////////////////////////////
        //Display related or non-sequitur emoji on each button
        /////////////////////////////////////////////////////////
        #region emojiDisplay
        for (int i = 0; i < responseButtons.Length; i++)
        {
            if (isButtonNonSequitur[i])
            {
                buttonEmoji[i] = ConvoGenenerator.ShowRandomEmoji(responseButtons[i].GetComponentInChildren<Text>());
            }
            else
            {
                buttonEmoji[i] = ConvoGenenerator.ShowRelatedEmoji(responseButtons[i].GetComponentInChildren<Text>(), lastEmoji);
            }
        }
        #endregion

        //Now we're waiting for a response from the player
        WaitingOnPlayerResponse = true;
        RemainingWaitTime = PlayerResponseWaitTime;


    }

    public void PlayerResponseSelection(int PlayerResponse)
    {
        //Shift PlayerResponse to properly correspond to the index of buttonEmoji
        PlayerResponse = PlayerResponse - 1;

        //Tell ConvoGenerator which emoji the player selected
        ConvoGenenerator.PlayerResponse(buttonEmoji[PlayerResponse]);

        //No longer waiting on player response
        WaitingOnPlayerResponse = false;

        //Hide the PlayerInputPanel
        PlayerInputPanel.SetActive(false);

    }

    private void PlayerResponseTimeOut()
    {
        //Tell ConvoGenerator the player didn't pick anything
        ConvoGenenerator.PlayerResponseTimeout();

        //No longer waiting on player response
        WaitingOnPlayerResponse = false;

        //Hide the PlayerInputPanel
        PlayerInputPanel.SetActive(false);

    }
}
