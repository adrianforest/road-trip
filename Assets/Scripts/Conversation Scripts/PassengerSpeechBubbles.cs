﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PassengerSpeechBubbles : MonoBehaviour {

    [SerializeField]
    private GameObject[] SpeechBubbles;
    [SerializeField]
    private Text[] PassengerTextFields;
      
    
    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void ActivatePassengerSpeech(int pos)
    {
        SpeechBubbles[pos].SetActive(true);
        PassengerTextFields[pos].enabled = true;
    }

    public void DeactivatePassengerSpeech(int pos)
    {
        SpeechBubbles[pos].SetActive(false);
        PassengerTextFields[pos].enabled = false;
    }

    public Text GetPassengerTextField(int pos)
    {
        Text tempText = PassengerTextFields[pos];
        return tempText;
    }

    public void ClearAllSpeech()
    {
        for (int i = 0; i < SpeechBubbles.Length; i++)
        {
            SpeechBubbles[i].SetActive(false);
        }

        for (int i = 0; i < PassengerTextFields.Length; i++)
        {
            PassengerTextFields[i].enabled = false;
        }
    }
}
