﻿using UnityEngine;
using System.Collections;

public class CarColorRandomiser : MonoBehaviour {

    [SerializeField]
    private Renderer CarMesh;
    [SerializeField]
    private Material[] Colors;

	// Use this for initialization
	void Start () {

        CarMesh.material = Colors[Random.Range(0,Colors.Length)];
        	
	}
	
	
}
