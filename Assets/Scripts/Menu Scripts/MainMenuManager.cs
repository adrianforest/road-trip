﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour {

    [SerializeField]
    GameObject MainPanel;
    [SerializeField]
    GameObject OptionsPanel;

    [SerializeField]
    GameObject LoadingPanel;

    [SerializeField]
    string startScene; //The first scene of the game, loaded on clicking Start
    [SerializeField]
    public GameObject ExitConfirmation; //The Exit Confirmation dialog panel


    // Use this for initialization
    void Start()
    {
        OptionsPanel.SetActive(false);
        ExitConfirmation.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    // Load starting Scene
    public void StartFirstScene()
    {
        MainPanel.SetActive(false);
        LoadingPanel.SetActive(true);
        PlayerPrefs.Save();
        SceneManager.LoadSceneAsync(startScene, LoadSceneMode.Single);
    }

    public void ShowOptions()
    {
        OptionsPanel.SetActive(true);
        MainPanel.SetActive(false);
    }

    public void ExitGame()
    {
        Debug.Log("PlayerPrefs VolumeLevel set on Exit to " + PlayerPrefs.GetFloat("MusicVolume"));
        PlayerPrefs.Save();
        ExitConfirmation.SetActive(true);
        MainPanel.SetActive(false);
    }
}
