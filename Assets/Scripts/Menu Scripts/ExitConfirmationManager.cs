﻿using UnityEngine;
using System.Collections;

public class ExitConfirmationManager : MonoBehaviour {

    public GameObject PreviousMenuPanel; //The previous panel;
    public GameObject ExitConfirmationPanel; //The exit confirmation panel

    public void ExitConfirm()
    {
        Application.Quit();
    }

    public void ExitCancel()
    {
        PreviousMenuPanel.SetActive(true);
        ExitConfirmationPanel.SetActive(false);
    }
}
