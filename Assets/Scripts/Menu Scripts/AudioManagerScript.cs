﻿using UnityEngine;
using System.Collections;

public class AudioManagerScript : MonoBehaviour {


    [SerializeField]
    AudioSource MainAudioSource;
    [Range(0, 100)]
    [SerializeField] private float MusicVolumeLevel;

    // Use this for initialization
    void Start () {
        //Debug.Log("AudioManager running Start");

        /////////////////////////////////////////////////////////////////////////////
        //      Initialise Audio Preferences
        ////////////////////////////////////////////////////////////////////////////

        //Check for Music Volume preference and get, if none exists, set to default
        if (PlayerPrefs.HasKey("MusicVolume"))
        {
            //Debug.Log("PlayerPrefs MusicVolume detected in AudioManager as " + PlayerPrefs.GetFloat("MusicVolume"));
            MusicVolumeLevel = PlayerPrefs.GetFloat("MusicVolume");
        }
        else
        {
            //Debug.Log("PlayerPrefs MusicVolume NOT detected in AudioManager");
            PlayerPrefs.SetFloat("MusicVolume", MusicVolumeLevel);
        }

        MainAudioSource.volume = MusicVolumeLevel;
        //Debug.Log("AudioManager completed Start");
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SetMusicVolumeLevel(float newLevel)
    {
        MusicVolumeLevel = newLevel;
        MainAudioSource.volume = MusicVolumeLevel;
        PlayerPrefs.SetFloat("MusicVolume", MusicVolumeLevel);
        //Debug.Log("PlayerPrefs MusicVolume set in AudioManager to " + PlayerPrefs.GetFloat("MusicVolume"));
    }

    public float GetMusicVolumeLevel()
    {
        if (PlayerPrefs.HasKey("MusicVolume"))
        {
            //Debug.Log("PlayerPrefs MusicVolume detected in AudioManager as " + PlayerPrefs.GetFloat("MusicVolume"));
            MusicVolumeLevel = PlayerPrefs.GetFloat("MusicVolume");
        }
        else
        {
            //Debug.Log("PlayerPrefs MusicVolume NOT detected in AudioManager");
            PlayerPrefs.SetFloat("MusicVolume", MusicVolumeLevel);
        }

        MainAudioSource.volume = MusicVolumeLevel;
        //Debug.Log("AudioManager completed GetMusicVolumeLevel");
        return MusicVolumeLevel;
    }
}
