﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class OptionsMenuScript : MonoBehaviour {

    #region declarations

    [SerializeField]
    private float UIElementGrey = 0.5f;

    #region panels

    [Header("Menu Panels")]
    [SerializeField]
    private GameObject PreviousMenuPanel; //The menu panel that the Options screen is accessed from
    [SerializeField]
    private GameObject OptionsPanel; //The options panel

    #endregion

    #region audio


    [Header("Audio")]
    //[Space10)]
    [SerializeField]
    private Slider MusicVolumeSlider; //master audio slider
    private float MusicVolumeLevel = 100f; // The music volume level

    [SerializeField]
    private AudioManagerScript AudioManager; //The Audio Manager that controls volume levels

    #endregion

    #region visuals


    [Header("Visuals")]
    //[Space(10)]
    [SerializeField]
    private Toggle SimpsonwaveToggle; //Simpsonwave toggle
    private bool SimpsonwaveState; //Simpsonwave state


    [SerializeField]
    private GameObject VFXGreyOut; //Panel hiding VFX options

    [SerializeField]
    private Toggle EdgeToggle; //Edge Detection toggle
    private bool EdgeDetectionState; //Edge Detection state

    [SerializeField]
    private Slider GlitchStrength; //Glitch Strength Slider
    private int GlitchStrengthIndex; //Glitch Strength Index

    //Effect Strength index constants
    private const int GLoff = 0;
    private const int GLvlow = 1;
    private const int GLlow = 2;
    private const int GLnorm = 3;
    private const int GLhigh = 4;
    private const int GLvhigh = 5;

    [SerializeField]
    private VisualsManagerScript VisualsManager;

    #endregion

    #region conversation

    [Header("Conversation")]
    //[Space(10)]

    [SerializeField]
    private Toggle PassengerToggle;

    [SerializeField]
    private GameObject ConversationGreyOut; //Panel blocking player conversation options

    [SerializeField]
    private Toggle ConversationModeToggle;

    private Color ConversationBackgroundDefault;
    private Color ConversationTextDefault;

    [SerializeField]
    private GameObject PlayerInputGreyOut; //Panel blocking player input options

    [SerializeField]
    private Toggle PlayerInputToggle;

    private Color PlayerInputBackgroundDefault;
    private Color PlayerInputTextDefault;

    [SerializeField]
    private ConversationGenerator ConvoGenerator;

    private bool PassengerStatus;
    private bool ConvoStatus;
    private bool PlayerInputStatus;

    #endregion

    //private bool FirstUpdate = true;

    #endregion


    // Use this for initialization
    void Start () {

        PlayerInputBackgroundDefault = PlayerInputToggle.GetComponentInChildren<Image>().color;
        PlayerInputTextDefault = PlayerInputToggle.GetComponentInChildren<Text>().color;

        ConversationBackgroundDefault = ConversationModeToggle.GetComponentInChildren<Image>().color;
        ConversationTextDefault = ConversationModeToggle.GetComponentInChildren<Text>().color;

        OptionsPanel.SetActive(false); //turn off the options menu

        #region Initialise Simpsonwave Mode

        //Check for Simpsonwave preference and get, if not, set to false
        if (PlayerPrefs.HasKey("Simpsonwave"))
        {
            if (PlayerPrefs.GetString("Simpsonwave") == "True")
            {
                
                SimpsonwaveState = true;
            }
            else
            {

                SimpsonwaveState = false;
            }
        }
        else
        {
            SimpsonwaveState = SimpsonwaveToggle.isOn;
        }

        //Check for Edge Detection preference and get, if not, set to default for SimpsonwaveState
        if (PlayerPrefs.HasKey("EdgeDetectionState"))
        {
            if (PlayerPrefs.GetString("EdgeDetectionState") == "True")
            {

                EdgeDetectionState = true;
            }
            else
            {

                EdgeDetectionState = false;
            }
        }
        else
        {
            EdgeDetectionState = EdgeToggle.isOn;
        }

        //Check for Glitch Strength preference and get, if not, set to default
        if (PlayerPrefs.HasKey("GlitchStrength"))
        {
            GlitchStrengthIndex = PlayerPrefs.GetInt("GlitchStrength");
        }
        else
        {
            GlitchStrengthIndex = (int)GlitchStrength.value;
        }

        //Make sure GlitchStrengthIndex is within the allowable range
        if (GlitchStrengthIndex > GLvhigh)
        {
            GlitchStrengthIndex = GLvhigh;
        }

        if (GlitchStrengthIndex < GLoff)
        {
            GlitchStrengthIndex = GLoff;
        }

        SimpsonwaveToggle.isOn = SimpsonwaveState;
        PlayerPrefs.SetString("Simpsonwave", SimpsonwaveState.ToString());

        VFXGreyOut.SetActive(!SimpsonwaveState);

        EdgeToggle.isOn = EdgeDetectionState;
        PlayerPrefs.SetString("EdgeDetectionState", EdgeDetectionState.ToString());

        GlitchStrength.value = GlitchStrengthIndex;
        PlayerPrefs.SetInt("GlitchStrength", GlitchStrengthIndex);

        if (SimpsonwaveState)
        {
            VisualsManager.EnableSimpsonwave();
            VisualsManager.setEdgeDetection(EdgeDetectionState);
        }
        else
        {
            VisualsManager.DisableSimpsonwave();
            VisualsManager.setEdgeDetection(false);
        }

        

        VisualsManager.setGlitchStrength(GlitchStrengthIndex);

        if(ConvoGenerator != null)
        {
            ConvoGenerator.setSimpsonwaveStatus(SimpsonwaveState);
        }

        #endregion

        #region Initialise Audio Options

        //Check for Music Volume preference from AudioManager
        //Debug.Log("Options getting MusicVolumeLevel from AudioManager = " + AudioManager.GetMusicVolumeLevel());
        MusicVolumeLevel = AudioManager.GetMusicVolumeLevel();

        MusicVolumeSlider.value = MusicVolumeLevel;

        #endregion

        #region Initialise Conversation

        #region Initialise ConversationMode

        //Check for Conversation Mode preference and get, if not, set to UI
        if (PlayerPrefs.HasKey("ConversationMode"))
        {
            
            if (PlayerPrefs.GetString("ConversationMode") == "True")
            {

                ConvoStatus = true;
            }
            else
            {

                ConvoStatus = false;
            }
        }
        else
        {
            ConvoStatus = ConversationModeToggle.isOn;
        }

        ConversationModeToggle.isOn = ConvoStatus;
        setPlayerInputGreyOut(!ConvoStatus);

        PlayerPrefs.SetString("ConversationMode", ConvoStatus.ToString());

        if (ConvoGenerator != null)
        {
            ConvoGenerator.SetConversationMode(ConvoStatus);
        }

        #endregion

        #region Initialise PlayerInputStatus

        //Check for PlayerInput preference and get, if not, set to false
        if (PlayerPrefs.HasKey("PlayerInputStatus"))
        {

            if (PlayerPrefs.GetString("PlayerInputStatus") == "True")
            {

                PlayerInputStatus = true;
            }
            else
            {

                PlayerInputStatus = false;
            }
        }
        else
        {
            PlayerInputStatus = PlayerInputToggle.isOn;
        }

        PlayerInputToggle.isOn = PlayerInputStatus;
        PlayerPrefs.SetString("PlayerInputStatus", PlayerInputStatus.ToString());

        if (ConvoGenerator != null)
        {
            ConvoGenerator.SetPlayerInput(PlayerInputStatus);
        }

        #endregion

        #region Initialise Passengers

        //Check for Passengers preference and get, if not, set to UI
        if (PlayerPrefs.HasKey("PassengersOn"))
        {

            if (PlayerPrefs.GetString("PassengersOn") == "True")
            {

                PassengerStatus = true;
            }
            else
            {

                PassengerStatus = false;
            }
        }
        else
        {
            PassengerStatus = PassengerToggle.isOn;
        }

        PassengerToggle.isOn = PassengerStatus;

        setConversationGreyOut(!PassengerStatus);
        ConversationModeToggle.isOn = ConvoStatus;

        if (PassengerStatus)
        {
            setPlayerInputGreyOut(!ConvoStatus);
        }
        else
        {
            setPlayerInputGreyOut(!PassengerStatus);
        }

        PlayerInputToggle.isOn = PlayerInputStatus;

        PlayerPrefs.SetString("PassengersOn", PassengerStatus.ToString());

        if (ConvoGenerator != null)
        {
            if (!ConvoGenerator.GetConvoInitialised())
            {
                ConvoGenerator.InitialiseConversationMode();
            }

            if (PassengerStatus)
            {
                ConvoGenerator.SetConversationMode(ConvoStatus);
            }
            else
            {
                ConvoGenerator.SetConversationMode(false);
            }

            ConvoGenerator.setShowPassengers(PassengerStatus);
        }
        
        #endregion

        #endregion


        //Debug.Log("Options Initialisation Complete");
    }


    //void Update()
    //{
    //    if (FirstUpdate)
    //    {
    //        FirstUpdate = false;

            
    //    }
    //}

    #region Audio Control Updates

    //Music Volume Level change
    public void UpdateMusicVolumeLevel()
    {
        MusicVolumeLevel = MusicVolumeSlider.value;
        AudioManager.SetMusicVolumeLevel(MusicVolumeLevel);
    }

    #endregion

   
    #region Visual Control Updates

    //Invert Simpsonwave Toggle
    public void ToggleSimpsonwave()
    {
        SimpsonwaveState = SimpsonwaveToggle.isOn;
        PlayerPrefs.SetString("Simpsonwave", SimpsonwaveState.ToString());
        if (SimpsonwaveState)
        {
            VisualsManager.EnableSimpsonwave();
        }
        else
        {
            VisualsManager.DisableSimpsonwave();
        }

        if (SimpsonwaveState)
        {
            VisualsManager.setEdgeDetection(EdgeDetectionState);
        }

        if (ConvoGenerator != null)
        {
            ConvoGenerator.setSimpsonwaveStatus(SimpsonwaveState);
        }

        VFXGreyOut.SetActive(!SimpsonwaveState);


    }

    public bool GetSimpsonwaveState()
    {
        return SimpsonwaveState;
    }

    public void ToggleEdgeDetection()
    {
        EdgeDetectionState = EdgeToggle.isOn;
        PlayerPrefs.SetString("EdgeDetectionState", EdgeDetectionState.ToString());

        VisualsManager.setEdgeDetection(EdgeDetectionState);
    }

    public void GlitchStrengthChange()
    {
        GlitchStrengthIndex = (int)GlitchStrength.value;

        PlayerPrefs.SetInt("GlitchStrength", GlitchStrengthIndex);

        VisualsManager.setGlitchStrength(GlitchStrengthIndex);
    }

    #endregion

    #region Conversation Control Updates

    public void TogglePassengers()
    {
        PassengerStatus = PassengerToggle.isOn;

        setConversationGreyOut(!PassengerStatus);
        ConversationModeToggle.isOn = ConvoStatus;

        if (PassengerStatus)
        {
            setPlayerInputGreyOut(!ConvoStatus);
        }
        else
        {
            setPlayerInputGreyOut(!PassengerStatus);
        }

        PlayerInputToggle.isOn = PlayerInputStatus;

        PlayerPrefs.SetString("PassengersOn", PassengerStatus.ToString());

        if (ConvoGenerator != null)
        {
            if (!ConvoGenerator.GetConvoInitialised())
            {
                ConvoGenerator.InitialiseConversationMode();
            }

            if (PassengerStatus)
            {
                ConvoGenerator.SetConversationMode(ConvoStatus);
            }
            else
            {
                ConvoGenerator.SetConversationMode(false);
            }

            ConvoGenerator.setShowPassengers(PassengerStatus);
            
        }
    }

    public void ToggleConversationMode()
    {

        ConvoStatus = ConversationModeToggle.isOn;

        setPlayerInputGreyOut(!ConvoStatus);
        PlayerInputToggle.isOn = PlayerInputStatus;



        PlayerPrefs.SetString("ConversationMode", ConvoStatus.ToString());
        
        if (ConvoGenerator != null)
        {
            ConvoGenerator.SetConversationMode(ConvoStatus);
        }
        
    }

    public void TogglePlayerInput()
    {
        PlayerInputStatus = PlayerInputToggle.isOn;

        PlayerPrefs.SetString("PlayerInputStatus", PlayerInputStatus.ToString());

        if (ConvoGenerator != null)
        {
            ConvoGenerator.SetPlayerInput(PlayerInputStatus);
        }

    }

    #endregion

    public void Back()
    {
        PreviousMenuPanel.SetActive(true);
        OptionsPanel.SetActive(false);
    }

    private void setPlayerInputGreyOut(bool newStatus)
    {
        if (newStatus)
        {
            Color tempColor = new Color(UIElementGrey, UIElementGrey, UIElementGrey);
            PlayerInputToggle.GetComponentInChildren<Image>().color = tempColor;
            PlayerInputToggle.GetComponentInChildren<Text>().color = tempColor;
        }
        else
        {
            PlayerInputToggle.GetComponentInChildren<Image>().color = PlayerInputBackgroundDefault;
            PlayerInputToggle.GetComponentInChildren<Text>().color = PlayerInputTextDefault;
        }

        PlayerInputGreyOut.SetActive(newStatus);
    }

    private void setConversationGreyOut(bool newStatus)
    {
        if (newStatus)
        {
            Color tempColor = new Color(UIElementGrey, UIElementGrey, UIElementGrey);
            ConversationModeToggle.GetComponentInChildren<Image>().color = tempColor;
            ConversationModeToggle.GetComponentInChildren<Text>().color = tempColor;
            
        }
        else
        {
            ConversationModeToggle.GetComponentInChildren<Image>().color = ConversationBackgroundDefault;
            ConversationModeToggle.GetComponentInChildren<Text>().color = ConversationTextDefault;

            setPlayerInputGreyOut(ConvoStatus);
        }
        
        
    }
}
