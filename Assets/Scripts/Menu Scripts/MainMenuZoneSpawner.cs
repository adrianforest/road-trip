﻿using UnityEngine;
using System.Collections;

public class MainMenuZoneSpawner : MonoBehaviour {


    [SerializeField]
    public float StartingWidth;

    public Renderer GroundPlane;

    public BackdropZone[] Zones;

    
    private BackdropZone currentZone;

    
    // Use this for initialization
    void Start () {
        BackdropZone prefab = Zones[Random.Range(0, Zones.Length)];
        currentZone = prefab.GetPooledInstance<BackdropZone>();
        GroundPlane.material.color = currentZone.groundPlaneMaterial.color;
        currentZone.PreSpawn(StartingWidth);
        
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
