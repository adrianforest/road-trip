﻿using UnityEngine;
using System.Collections;

public class PauseMenuManager : MonoBehaviour {

    [SerializeField]
    public GameObject PauseMenuPanel; //The pause menu panel
    [SerializeField]
    public GameObject OptionsPanel; //The options panel
    [SerializeField]
    public GameObject ExitConfirmation; //The exit confirmation panel
    [SerializeField]
    public Canvas ConversationCanvas; //The conversation canvas

    private bool ConversationModeOn;
    private bool disableEscKey = false;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && !disableEscKey)
        {
            disableEscKey = true;

            if (!PauseMenuPanel.activeSelf)
            {
                if (ConversationCanvas.enabled)
                {
                    ConversationModeOn = true;
                    ConversationCanvas.enabled = false;
                }
                    
                PauseMenuPanel.SetActive(true);
            }
            
        }
    }

    public void Resume()
    {
        PauseMenuPanel.SetActive(false);
        disableEscKey = false;
        if (ConversationModeOn)
        {
            ConversationCanvas.enabled = true;
        }
    }

    public void ShowOptions()
    {
        OptionsPanel.SetActive(true);
        PauseMenuPanel.SetActive(false);
    }

    public void ExitGame()
    {
        ExitConfirmation.SetActive(true);
        PauseMenuPanel.SetActive(false);
    }
}
