﻿using UnityEngine;

public class MainMenuMountainSpawner : MonoBehaviour {

    [SerializeField]
    public float StartingWidth;

    [SerializeField]
    public FloatRange timeBetweenSpawns, scale;
    [SerializeField]
    public float currentSpeed;
    [SerializeField]
    public BackdropObject[] objectPrefabs;
    [SerializeField]
    float timeSinceLastSpawn;
    //[SerializeField]
    //float currentSpawnDelay;
    
    [SerializeField]
    public int ZoneArrayIndex { get; set; }
    [SerializeField]
    public float PreTransitionMaxInterval { get; set; }
    [SerializeField]
    public float PreTransitionMinInterval { get; set; }
    [SerializeField]
    public float PreTransitionMaxScale { get; set; }


    void SpawnObject(Vector3 spawnLocation)
    {
        int spawningObjectIndex = Random.Range(0, objectPrefabs.Length);

        BackdropObject prefab = objectPrefabs[spawningObjectIndex];
        BackdropObject spawn = prefab.GetPooledInstance<BackdropObject>();

        spawn.transform.localPosition = spawnLocation;
        Vector3 scaleMultiplier = Vector3.one * scale.RandomInRange;
        scaleMultiplier.Scale(objectPrefabs[spawningObjectIndex].transform.localScale);
        spawn.transform.localScale = scaleMultiplier;
        spawn.setSpeed(0f);
    }

    //void OnEnable ()
    //{
    //    currentSpawnDelay = timeBetweenSpawns.RandomInRange;
    //}

    void Start()
    {
        //currentSpawnDelay = timeBetweenSpawns.RandomInRange;

        float currentDistance = 0f;
        Vector3 currentPosition = transform.position;

        while (currentDistance < StartingWidth)
        {
            currentDistance = currentDistance + (timeBetweenSpawns.RandomInRange * 1.25f);
            currentPosition.x = currentPosition.x + currentDistance;
            SpawnObject(currentPosition);
        }
    }

        
}