﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;
using Kino;

public class VisualsManagerScript : MonoBehaviour {

    [SerializeField]
    private Camera MainCamera;
    [SerializeField]
    private bool SimpsonwaveState;
    [SerializeField]
    private OptionsMenuScript Options;

    [SerializeField]
    [HideInInspector]
    private SimpsonwaveEffects SimpsonwaveEffectsScript;
    [SerializeField]
    [HideInInspector]
    private EdgeDetection EdgeDetectionScript;
    [SerializeField]
    [HideInInspector]
    private AnalogGlitch AnalogGlitchScript;
    [SerializeField]
    [HideInInspector]
    private DigitalGlitch DigitalGlitchScript;
    [SerializeField]
    [HideInInspector]
    private VignetteAndChromaticAberration VignetteAndChromaticAberrationScript;


    // Use this for initialization
    void Awake () {

        SimpsonwaveEffectsScript = MainCamera.GetComponent<SimpsonwaveEffects>();
        EdgeDetectionScript = MainCamera.GetComponent<EdgeDetection>();
        AnalogGlitchScript = MainCamera.GetComponent<AnalogGlitch>();
        DigitalGlitchScript = MainCamera.GetComponent<DigitalGlitch>();
        VignetteAndChromaticAberrationScript = MainCamera.GetComponent<VignetteAndChromaticAberration>();

        //Check for Simpsonwave preference and get, if not, set to false
        if (PlayerPrefs.HasKey("Simpsonwave"))
        {
            //Debug.Log("PlayerPrefs Simpsonwave pref detected in VisualsManager as " + PlayerPrefs.GetString("Simpsonwave"));
            if (PlayerPrefs.GetString("Simpsonwave") == "True")
            {

                SimpsonwaveState = true;
            }
            else
            {

                SimpsonwaveState = false;
            }
            //Debug.Log("Simpsonwave state in VisualsManager = " + SimpsonwaveState.ToString());
        }
        else
        {
            //Debug.Log("No PlayerPrefs Simpsonwave pref detected in VisualsManager");
            SimpsonwaveState = false;
            //Debug.Log("PlayerPrefs Simpsonwave = " + SimpsonwaveState.ToString());
        }

        PlayerPrefs.SetString("Simpsonwave", SimpsonwaveState.ToString());

        //Get SimpsonwaveState from OptionsMenuScript
        //SimpsonwaveState = Options.GetSimpsonwaveState();

        if (SimpsonwaveState)
        {
            EnableSimpsonwave();
        }
        else
        {
            DisableSimpsonwave();
        }
        //Debug.Log("VisualsManager Simpsonwave = " + SimpsonwaveState.ToString());

        //Debug.Log("VisualsManager Initialisation Complete");
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void EnableSimpsonwave()
    {
        SimpsonwaveState = true;
        SimpsonwaveEffectsScript.enabled = true;
        EdgeDetectionScript.enabled = true;
        AnalogGlitchScript.enabled = true;
        DigitalGlitchScript.enabled = true;
        VignetteAndChromaticAberrationScript.enabled = true;
        //Debug.Log("Simpsonwave in VisualsManager = " + SimpsonwaveState.ToString());
    }

    public void DisableSimpsonwave()
    {
        SimpsonwaveState = false;
        SimpsonwaveEffectsScript.enabled = false;
        EdgeDetectionScript.enabled = false;
        AnalogGlitchScript.enabled = false;
        DigitalGlitchScript.enabled = false;
        VignetteAndChromaticAberrationScript.enabled = false;
        //Debug.Log("Simpsonwave  in VisualsManager = " + SimpsonwaveState.ToString());
    }

    public void setEdgeDetection(bool newStatus)
    {
        EdgeDetectionScript.enabled = newStatus;
    }

    public bool getEdgeDetection()
    {
        return EdgeDetectionScript.enabled;
    }

    public void setGlitchStrength(int index)
    {
        SimpsonwaveEffectsScript.SetEffectStrength(index);
    }

    //public int getGlitchStrength()
    //{
    //    return SimpsonwaveEffectsScript
    //}
}
