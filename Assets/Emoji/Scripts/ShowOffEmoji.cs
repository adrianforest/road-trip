﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Text;
using UnityEngine.UI;

public class ShowOffEmoji : MonoBehaviour {

	//The text file that contains the co-ordinates in the atlas for each Unicode emoji
    public TextAsset textAsset;
    
	/// UI Text fields that this script outputs to
	public Text bicycleAndUSFlagText;
    public Text footballText;
    public Text receipeText;

	//The RawImage that holds the emoji.
	//This gets cloned and used to display the emoji by setting it to the offset corresponding to that emoji
    public RawImage rawImageToClone;

	//This dictionary holds the positions of the each emoji on the emoji atlas texture, indexed by 
    private Dictionary<string, Rect> emojiRects = new Dictionary<string, Rect>();

	//This is the character used to create a space for the emoji
    private static char emSpace = '\u2001';

    void Awake()
    {
        #if !UNITY_EDITOR && UNITY_WEBGL
        WebGLInput.captureAllKeyboardInput = false;
        #endif
    }

    // Use this for initialization
    void Start ()
    {
		//This sends the text file to ParseEmojiInfo to be put into the Dictionary
        this.ParseEmojiInfo(this.textAsset.text);

		//These lines just send hardcoded strings to be converted into strings displayed on UI text elements
        StartCoroutine(this.SetUITextThatHasEmoji(this.bicycleAndUSFlagText, "bicyclist: \U0001F6B4, and US flag: \U0001F1FA\U0001F1F8"));
        StartCoroutine(this.SetUITextThatHasEmoji(this.footballText, "⚽ ➕ ❤ = I love football"));
        StartCoroutine(this.SetUITextThatHasEmoji(this.receipeText, "\U0001F3B5 \U0001F3B6 \U0001F3B7 \U0001F3B8 \U0001F3B9 \U0001F3BA"));
    }

	//This function takes a string and converts it to a UTF-16 encoded string
	// This used to convert the unicode hexadecimal values from the emoji atlas' index file into decimal values stored as strings
    private static string GetConvertedString(string inputString)
    {
		//Create an array to hold parts of a string split at the '-' character
        string[] converted = inputString.Split('-');

		//Iterate through the array of strings
        for (int j = 0; j < converted.Length; j++)
        {
			//Convert the string from a hexadecimal value to a UTF-16 encoded string
            converted[j] = char.ConvertFromUtf32(Convert.ToInt32(converted[j], 16));
        }
		//returns a string consisting of all the strings joined together with empty characters
        return string.Join(string.Empty, converted);
    }

	//This function takes the text asset and uses it to add the atlas positions of all the emojis to emojiRects
	//This is used to populate the dictionary with the atlas positions of all emojis,
	//indexed by their Unicode value (as a decimal string)
    private void ParseEmojiInfo(string inputString)
    {
		//Reading the strings
        using (StringReader reader = new StringReader(inputString))
        {
			//Create a string to hold the line we read
            string line = reader.ReadLine();
            
			//Iterate through each line of the text asset
			while (line != null && line.Length > 1)
            {
                // We add each emoji to emojiRects

				//Create an array that holds the line, split into different strings, separated by spaces
                string[] split = line.Split(' ');

				//Create a new float x based on the string at [1]
                float x = float.Parse(split[1], System.Globalization.CultureInfo.InvariantCulture);

				//Create a new float y based on the string at [2]
                float y = float.Parse(split[2], System.Globalization.CultureInfo.InvariantCulture);

				//Create a new float width based on the string at [3]
                float width = float.Parse(split[3], System.Globalization.CultureInfo.InvariantCulture);

				//Create a new float height based on the string at [4]
                float height = float.Parse(split[4], System.Globalization.CultureInfo.InvariantCulture);

				//Set the emojiRect element at the index of the Unicode value (as a decimal string)
				//to a Rect holding the position and size on the atlas of the corresponding emoji
                this.emojiRects[GetConvertedString(split[0])] = new Rect(x, y, width, height);

                line = reader.ReadLine();
            }
        }
    }

    private struct PosStringTuple
    {
        public int pos;
        public string emoji;

        public PosStringTuple(int p, string s)
        {
            this.pos = p;
            this.emoji = s;
        }
    }

    public void SetReceipeTextFromJavascript(string input)
    {
        foreach (Transform child in receipeText.transform)
        {
            Destroy(child.gameObject);
        }

        StartCoroutine(this.SetUITextThatHasEmoji(this.receipeText, input));
    }

	//This function takes a UI text element and a string, converts the unicode elements in the strong to emSpaces,
	//places the corresponding emoji at that position, then sets the text element to the resulting string
    public IEnumerator SetUITextThatHasEmoji(Text textToEdit, string inputString)
    {
		//create list to hold positions of emSpaces where emojis will be placed
        List<PosStringTuple> emojiReplacements = new List<PosStringTuple>();

		//???Create Stringbuilder for ???
        StringBuilder sb = new StringBuilder();

		//iterates through every character in the string
        int i = 0;
        while (i < inputString.Length)
        {
            string singleChar = inputString.Substring(i, 1);
            string doubleChar = "";
            string fourChar = "";

            if (i < (inputString.Length - 1))
            {
                doubleChar = inputString.Substring(i, 2);
            }

            if (i < (inputString.Length - 3))
            {
                fourChar = inputString.Substring(i, 4);
            }

			// Check 64 bit emojis first
            if (this.emojiRects.ContainsKey(fourChar))
            {
                //Append an emSpace to the StringBuilder, where the emoji will go
                sb.Append(emSpace);

				//Add the position of this emSpace to the emojiReplacements List, so we can position an emoji there later

				//Creates a new PosStringTuple with the positiong set to the current length of the StringBuilder, and
				//the emoji set to the unicode of the emoji as a string
                emojiReplacements.Add(new PosStringTuple(sb.Length - 1, fourChar));
                i += 4;
            }
			// Then check 32 bit emojis
            else if (this.emojiRects.ContainsKey(doubleChar))
            {
                
                sb.Append(emSpace);
                emojiReplacements.Add(new PosStringTuple(sb.Length - 1, doubleChar));
                i += 2;
            }
			// Finally check 16 bit emojis
            else if (this.emojiRects.ContainsKey(singleChar))
            {
                
                sb.Append(emSpace);
                emojiReplacements.Add(new PosStringTuple(sb.Length - 1, singleChar));
                i++;
            }
            else
            {
                sb.Append(inputString[i]);
                i++;
            }
        }

        // Set text
        textToEdit.text = sb.ToString();

        yield return null;

        // And spawn RawImages as emojis

		//Creates a TextGenerator based on the text field to be edited.
		//This is used to determine the final positions of the emoji images based on the position of the emSpaces in the UI Text
        TextGenerator textGen = textToEdit.cachedTextGenerator;

        // One rawimage per emoji
        for (int j = 0; j < emojiReplacements.Count; j++)
        {
			//Create int to 
            int emojiIndex = emojiReplacements[j].pos;
            GameObject newRawImage = GameObject.Instantiate(this.rawImageToClone.gameObject);
            newRawImage.transform.SetParent(textToEdit.transform);
            Vector3 imagePos = new Vector3(textGen.verts[emojiIndex * 4].position.x, textGen.verts[emojiIndex * 4].position.y, 0);
            newRawImage.transform.localPosition = imagePos;

            RawImage ri = newRawImage.GetComponent<RawImage>();
            ri.uvRect = emojiRects[emojiReplacements[j].emoji];
        }
    }
}
