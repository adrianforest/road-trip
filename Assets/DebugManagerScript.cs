﻿using UnityEngine;
using System.Collections;

public class DebugManagerScript : MonoBehaviour {

    public Canvas debugMenu;

	// Use this for initialization
	void Start () {

        debugMenu.enabled = false;


    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown("space"))
            debugMenu.enabled = !debugMenu.enabled;

    }
}
